" VIM-PLUG {{{
" ========
" first:
"   curl -fLo ~/.dotfiles/nvim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

call plug#begin('$HOME/.dotfiles/nvim/plugged')
Plug 'https://gitlab.com/aimebertrand/timu-spacegrey-vim.git'
Plug 'https://gitlab.com/aimebertrand/timu-rouge-vim.git'
Plug 'axvr/org.vim'
Plug 'chrisbra/colorizer'
Plug 'easymotion/vim-easymotion'
Plug 'edkolev/tmuxline.vim'
Plug 'haya14busa/is.vim'
Plug 'itchyny/lightline.vim'
Plug 'jreybert/vimagit'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-peekaboo'
Plug 'liuchengxu/vim-which-key'
Plug 'ryanoasis/vim-devicons'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'vim-python/python-syntax'
Plug 'voldikss/vim-floaterm'
call plug#end()

" auto install missing plugins
autocmd VimEnter *
  \  if !empty(filter(copy(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall
  \| endif
" }}}

" GENERAL SETTINGS {{{
" ================
" no need for compatibility with vi:
set nocompatible
" auto change cwd:
set autochdir
" highlight substitution:
if has('nvim')
 set inccommand=nosplit
endif
" folds:
function! MyFoldText()
  let line = getline(v:foldstart)
  let foldedlinecount = v:foldend - v:foldstart + 1
  return ' ► '. foldedlinecount . line
endfunction
set foldtext=MyFoldText()
set fillchars=fold:\-
set foldmethod=marker
" show hidden characters:
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list
" path for viminfo:
if has('nvim')
  set viminfo+=n~/.dotfiles/nvim/nviminfo
endif
if !has('nvim')
  set viminfo+=n~/.dotfiles/nvim/viminfo
endif
" cursor behavior per mode:
let &t_SI.="\033[6 q"
let &t_EI.="\033[2 q"
" }}}

" COLORS AND FONTS {{{
" ================
" terminal color settings:
if !has('nvim')
  set term=$TERM
endif
set termguicolors
" enable syntax highlighting:
syntax enable
" dark mode enabled?:
set background=dark
" color scheme:
colorscheme timu-spacegrey-vim
" color scheme for lightline:
let g:lightline = {
  \ 'colorscheme': 'timu_spacegrey_vim',
  \ }
" }}}

" VIM USER INTERFACE {{{
" ==================
set title
set cursorline
set mouse=a
set noerrorbells visualbell t_vb=
" turn on the wild menu:
set wildmenu
" turn line numbers:
set number
set numberwidth=6
set relativenumber
" macos clipboar:
if has('nvim')
  set clipboard=unnamedplus
endif
if !has('nvim')
  set clipboard=unnamed
endif
" disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" }}}

" SEARCH {{{
" ======
" remove hl after searching:
let g:incsearch#auto_nohlsearch = 1
" show matching brackets when text indicator is over them:
set showmatch
" how many tenths of a second to blink when matching brackets:
set mat=2
" always show the status line:
set laststatus=2
" }}}

" FUNCTIONS {{{
" =========
" rename current file.
" from thanks gary bernhardt through ben orenstein:
function! RenameFile()
  let old_name = expand('%')
  let new_name = input('New file name: ', expand('%'), 'file')
  if new_name != '' && new_name != old_name
    exec ':saveas ' . new_name
    exec ':silent !rm ' . old_name
    redraw!
  endif
endfunction
" }}}

" GENERAL KEYBINDINGS {{{
" ===================
let mapleader = " "

" Files {{{
" -----
nmap <leader>r :source $MYVIMRC<CR>
" }}}

" Misc {{{
" ----
nmap <leader>s :w<CR>
nmap <leader>c :close<CR>
nmap <leader>w :bdel<CR>
nmap <leader>q :qa<CR>
nnoremap <C-,> :tabprevious<CR>
nnoremap <C-.> :tabnext<CR>
map <Leader>n :call RenameFile()<CR>
" }}}

" Windows {{{
" -------
" splitting windows with same buffer:
nmap <leader>vs :vsplit<CR><C-w><Right>
nmap <leader>hs :split <CR><C-w><Down>
" splitting windows with new empty buffer:
nmap <leader>vn :vnew<CR><C-w>L<C-w><Right>
nmap <leader>hn :new<CR><C-w>R<C-w><Down>
" moving cursor between windows:
map <C-Right> <C-w><Right>
map <C-Left> <C-w><Left>
map <C-Down> <C-w><Down>
map <C-Up> <C-w><UP>
" moving windows around:
map <leader><Right> <C-w>L
map <leader><Left> <C-w>R
" resizing windows:
nmap <M-Right>  :vertical resize +2<CR>
nmap <M-Left>  :vertical resize -2<CR>
nmap <M-Up>  :resize +2<CR>
nmap <M-Down>  :resize -2<CR>
" move with visual lines:
map <up> gk
map <down> gj
" change wildmenu bindings:
if &wildoptions =~ "pum"
    cnoremap <expr> <up> pumvisible() ? "<C-p>" : "\\<up>"
    cnoremap <expr> <down> pumvisible() ? "<C-n>" : "\\<down>"
endif
" }}}
" }}}

" PLUGINS SETTINGS & BINDINGS {{{
" ===========================
" vim-plug:
nmap <leader>pc :PlugClean<CR>
nmap <leader>pi :PlugInstall<CR>
nmap <leader>pu :PlugUpdate<CR>
nmap <leader>pg :PlugUpgrade<CR>

" colorizer:
nmap <leader>hh :ColorToggle<CR>

" easymotion:
map s <Plug>(easymotion-s)

" fzf plugin:
nmap <leader>o :Files<CR>
nmap <leader>f :Rg<CR>
nmap <leader>g :GFiles<CR>
nmap <leader>b :Buffers<CR>

let $FZF_DEFAULT_OPTS .= ' --reverse'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" floaterm & ranger:
let g:floaterm_opener = 'edit'
nmap <leader>n :FloatermNew  --height=0.9 --width=0.9 ranger<CR>

" org plugin:
" autocmd FileType org setlocal foldenable foldlevelstart=99
autocmd FileType org setlocal nofoldenable
map <Tab> za
map <S-Tab> zA

" python-syntax:
let g:python_highlight_all = 1

" goyo:
let g:goyo_linenr = 1

" vim-which-key:
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
set timeoutlen=200
" }}}
