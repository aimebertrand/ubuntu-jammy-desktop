;;; early-init.el --- Custom before init settings -*- lexical-binding: t -*-

;; Author: Aimé Bertrand
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2021-08-11
;; Keywords: emacs early init config
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; `early-init' file to load my settings befor Emacs `init'.
;; This includes the definition of packages to install, most of the
;; appearance settings for the Emacs frame and the default font to use.

;;; Code:


;;; PACKAGE PATHS
(add-to-list 'load-path (expand-file-name "packages" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "libraries" user-emacs-directory))


;;; APPEARANCE SETTINGS
(tool-bar-mode -1)
(scroll-bar-mode -1)
(pcase system-type
  ('gnu/linux (menu-bar-mode -1))
  ('windows-nt (menu-bar-mode -1)))

;; frame measurements
(customize-set-variable
 'initial-frame-alist
 '((top . 0) (left . 0.4) (width . 0.6) (height . 0.6)))

;; transparency
(set-frame-parameter (selected-frame) 'alpha '(100 . 100))

;; don't show default Emacs info
(customize-set-variable 'inhibit-startup-message t)

;; make frame resize exactly till the edges
(customize-set-variable 'frame-resize-pixelwise t)


;;; NATIVE COMPILATION
(setq native-comp-async-report-warnings-errors 'silent)


;;; EVIL IS EVIL
(setq evil-want-C-i-jump nil)
(setq evil-want-integration t)
(setq evil-want-keybinding nil)
(setq evil-kill-on-visual-paste nil)


;;; CUSTOM GROUP
(defgroup timu ()
  "Custom group for my own libraries."
  :group 'emacs
  :prefix "timu-")

(defcustom timu-libraries-path
  (expand-file-name "libraries/" user-emacs-directory)
  "Variable for the path to the directory with custom libraries."
  :type 'directory
  :group 'timu)


;;; PACKAGES TO BE INSTALLED
(defcustom timu-package-list
  '(0x0
    ace-link
    ace-window
    all-the-icons
    all-the-icons-dired
    all-the-icons-completion
    all-the-icons-ibuffer
    ansible
    ansible-doc
    apples-mode
    applescript-mode
    auto-dictionary
    auto-virtualenv
    avy
    browse-at-remote
    company-anaconda
    company-box
    company-emoji
    company-restclient
    company-shell
    consult
    csv-mode
    dash
    demap
    devdocs
    dired-filetype-face
    dired-narrow
    dired-subtree
    docker-compose-mode
    dockerfile-mode
    doom-modeline
    dwim-shell-command
    edit-indirect
    eglot
    eldoc-box
    elisp-demos
    embark
    embark-consult
    esxml
    evil
    evil-collection
    evil-leader
    evil-surround
    evil-terminal-cursor-changer
    exec-path-from-shell
    expenses
    flymake-diagnostic-at-point
    flymake-shellcheck
    flyspell-correct
    format-all
    git-gutter+
    git-modes
    go-mode
    grip-mode
    helpful
    htmlize
    imenu-list
    json-mode
    latex-preview-pane
    lsp-mode
    lsp-sourcekit
    lsp-ui
    lua-mode
    magit
    marginalia
    markdown-mode
    minions
    mixed-pitch
    multiple-cursors
    neotree
    noflet
    ob-applescript
    ob-async
    ob-http
    ob-restclient
    orderless
    org
    org-bullets
    org-contrib
    org-download
    org-remark
    org-web-tools
    package-lint
    paradox
    pip-requirements
    popper
    powershell
    project
    python-mode
    quelpa
    rainbow-delimiters
    rainbow-mode
    restclient
    s
    swift-mode
    syslog-mode
    timu-rouge-theme
    timu-spacegrey-theme
    transient
    treemacs-all-the-icons
    treemacs-icons-dired
    undo-tree
    vertico
    vimrc-mode
    web-mode
    websearch
    wgrep
    which-key
    writeroom-mode
    yaml-mode
    yasnippet)
  "List of packages to be installed for the Emacs config to work as configured.
The packages will be installed with `timu-func-install-packages'."
  :type '(repeat symbol)
  :group 'timu)


;;; early-init.el ends here
