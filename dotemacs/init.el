;;; init.el --- Custom settings -*- lexical-binding: t -*-

;; Author: Aimé Bertrand
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2021-08-11
;; Keywords: emacs init
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; `init' to load my settings into Emacs.
;; This includes declarations of `package-archives', installation of the
;; packages and the loading of the literate configuration.

;;; Code:


(require 'package)
(require 'cl-lib)


;;; SETUP PACKAGE SOURCES
(customize-set-variable 'package-enable-at-startup nil)

(pcase system-type
  ('windows-nt
   (customize-set-variable 'package-check-signature nil)))

(customize-set-variable 'package-archives
                        '(("melpa" . "https://melpa.org/packages/")
                          ("gnu" . "https://elpa.gnu.org/packages/")
                          ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

(customize-set-variable 'package-archive-priorities
                        '(("melpa" . 9)
                          ("gnu" . 6)
                          ("nongnu" . 2)))

(package-initialize)


;;; SETUP QUELPA
(customize-set-variable 'quelpa-dir
                        (expand-file-name "local/quelpa" user-emacs-directory))
(customize-set-variable 'quelpa-checkout-melpa-p nil)

(unless (package-installed-p 'quelpa)
  (with-temp-buffer
    (url-insert-file-contents
     "https://raw.githubusercontent.com/quelpa/quelpa/master/quelpa.el")
    (eval-buffer)
    (quelpa-self-upgrade)))


;;; LOAD THE CONFIG
(toggle-debug-on-error)
(require 'timu-func)

;; run package installation
(timu-func-install-packages)

;; Because: This variable is used by `package-autoremove' to decide.
(customize-set-variable 'package-selected-packages
                        timu-package-list)

;; load custom libraries
(let ((default-directory user-emacs-directory)
      (file-name-handler-alist nil)
      (gc-cons-percentage .6)
      (gc-cons-threshold most-positive-fixnum)
      (read-process-output-max (* 1024 1024)))
  (timu-func-require-modules)
  (garbage-collect))

(toggle-debug-on-error)


;;; init.el ends here
