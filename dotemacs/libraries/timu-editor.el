;;; timu-editor.el --- Config for writing -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper writing editor
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for all thing writing.

;;; Code:


(require 'flyspell)
(require 'writeroom-mode)
(require 'company)
(require 'lsp)


;;; DEFAULTS
(defgroup timu-editor ()
  "Customise group for the `timu-editor' Library."
  :group 'timu)

(defcustom timu-editor-path
  (expand-file-name "libraries/timu-editor.el" user-emacs-directory)
  "Variable for the path of the module `timu-editor'."
  :type 'file
  :group 'timu-editor)


;;; FLYSPELL
(customize-set-variable 'ispell-program-name (executable-find "aspell"))

(add-hook 'flyspell-mode-hook
          (lambda ()
            (auto-dictionary-mode 1)))

;;;; flyspell-correct
(pcase system-type
  ('darwin (define-key flyspell-mode-map (kbd "C-ƒ") #'flyspell-correct-wrapper))
  ((or 'gnu/linux 'windows-nt)
   (define-key flyspell-mode-map (kbd "M-s-f") #'flyspell-correct-wrapper)))
(customize-set-variable 'flyspell-correct-interface #'flyspell-correct-completing-read)


;;; YASNIPPET
(yas-global-mode 1)

(customize-set-variable
 'yas-snippet-dirs
 (list (expand-file-name "snippets" user-emacs-directory)))


;;; MULTIPLE-CURSORS
(customize-set-variable
 'mc/list-file
 (expand-file-name "local/.mc-lists.el" user-emacs-directory))


;;; WRITEROOM-MODE
(customize-set-variable 'writeroom-mode-line t)
(customize-set-variable 'writeroom-width 120)
(customize-set-variable 'writeroom-fullscreen-effect 'maximized)

(pcase system-type
  ('darwin
   (define-key writeroom-mode-map (kbd "M-±") #'writeroom-increase-width)
   (define-key writeroom-mode-map (kbd "M-–") #'writeroom-decrease-width))
  ((or 'gnu/linux 'windows-nt)
   (define-key writeroom-mode-map (kbd "C-s-+") #'writeroom-increase-width)
   (define-key writeroom-mode-map (kbd "C-s--") #'writeroom-decrease-width)))


;;; EDITING KEYBINDINGS
(pcase system-type
  ('darwin
   (evil-define-key 'normal prog-mode-map (kbd "Ø") #'timu-func-insert-line-before)
   (evil-define-key 'normal prog-mode-map (kbd "ø") #'timu-func-insert-line-after)
   (evil-define-key 'normal text-mode-map (kbd "Ø") #'timu-func-insert-line-before)
   (evil-define-key 'normal text-mode-map (kbd "ø") #'timu-func-insert-line-after))
  ((or 'gnu/linux 'windows-nt)
   (evil-define-key 'normal prog-mode-map (kbd "s-O") #'timu-func-insert-line-before)
   (evil-define-key 'normal prog-mode-map (kbd "s-o") #'timu-func-insert-line-after)
   (evil-define-key 'normal text-mode-map (kbd "s-O") #'timu-func-insert-line-before)
   (evil-define-key 'normal text-mode-map (kbd "s-o") #'timu-func-insert-line-after)))

(add-hook 'after-init-hook
          (lambda ()
            (interactive)
            (bug-reference-mode)
            (bug-reference-mode)))


(provide 'timu-editor)

;;; timu-editor.el ends here
