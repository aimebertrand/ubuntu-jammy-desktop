;;; timu-major-modes.el --- Config for several major modes -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: languages tools helper major modes
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for several major modes
;; that do not warrant e separate module.

;;; Code:


(require 'edit-indirect)
(require 'elisp-demos)
(require 'expenses)
(require 'flymake-diagnostic-at-point)
(require 'markdown-mode)
(require 'ob-applescript)
(require 'wgrep)


;;; DEFAULTS
(defgroup timu-major-modes ()
  "Customise group for the `timu-major-modes' Library."
  :group 'timu)

(defcustom timu-major-modes-path
  (expand-file-name "libraries/timu-major-modes.el" user-emacs-directory)
  "Variable for the path of the module `timu-major-modes'."
  :type 'file
  :group 'timu-major-modes)


;;; GET HELP
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)


;;; WGREP
(define-key wgrep-mode-map (kbd "M-s") #'wgrep-finish-edit)
(define-key wgrep-mode-map (kbd "M-w") #'wgrep-abort-changes)


;;; EMACS LISP MODE
(define-key emacs-lisp-mode-map (kbd "M-≈") #'eval-defun)
(define-key lisp-interaction-mode-map (kbd "M-≈") #'eval-defun)



;;; OUTLINE MINOR MODE
(add-hook 'emacs-lisp-mode-hook #'outline-minor-mode)
(evil-define-key 'normal outline-minor-mode-map (kbd "<tab>") #'outline-toggle-children)
(evil-define-key 'normal outline-minor-mode-map (kbd "z a") #'outline-show-all)
(evil-define-key 'normal outline-minor-mode-map (kbd "z A") #'timu-func-outline-hide-sublevels)
(evil-define-key 'normal outline-minor-mode-map (kbd "z c") #'outline-hide-subtree)
(evil-define-key 'normal outline-minor-mode-map (kbd "z o") #'outline-show-subtree)
(define-key outline-minor-mode-map [remap outline-demote] #'org-insert-link)


;;; OCCUR
(define-key occur-edit-mode-map (kbd "M-s") #'occur-cease-edit)


;;; ANSIBLE
(add-hook 'yaml-mode-hook #'ansible-doc-mode)


;;; CSV
(add-to-list 'auto-mode-alist '("\\.csv\\'" . csv-mode))


;;; POWERSHELL
(add-to-list 'auto-mode-alist '("\\.ps[dm]?1\\'" . powershell-mode))


;;; YAML
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-mode))


;;; SYSLOG-MODE
(add-to-list 'auto-mode-alist '("\\.log\\'" . syslog-mode))


;;; VIMRC-MODE
(add-to-list 'auto-mode-alist '("\\.vim\\(rc\\)?\\'" . vimrc-mode))


;;; SHELL-SCRIPT-MODE
(add-to-list 'auto-mode-alist '("\\.zsh\\'" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zshrc" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zshenv" . sh-mode))
(add-to-list 'auto-mode-alist '("\\(\\.\\)?zprofile" . sh-mode))


;;; MARKDOWN
;;;; markdown-mode install and setup
(customize-set-variable 'markdown-command "multimarkdown")

(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))


;;;; edit-indirect for code blocks
(pcase system-type
  ('darwin
   (define-key markdown-mode-map (kbd "‘") #'markdown-edit-code-block)
   (define-key edit-indirect-mode-map (kbd "‘") #'edit-indirect-commit))
  ((or 'gnu/linux 'windows-nt)
   (define-key markdown-mode-map (kbd "s-$") #'markdown-edit-code-block)
   (define-key edit-indirect-mode-map (kbd "s-$") #'edit-indirect-commit)))


;;; WEB-MODE
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode))


;;; DOCKER
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))


;;; EXPENSES
(customize-set-variable 'expenses-currency "CHF")
(customize-set-variable 'expenses-directory "~/org/expenses/")
(customize-set-variable
 'expenses-category-list
 '("Crafting" "Electronics" "Entertainment" "Food" "Grocery" "Health"
   "Others" "Rent" "Salary" "Shopping" "Subscription" "Travel"))
(customize-set-variable
 'expenses-month-names
 '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))


;;; ERC
(customize-set-variable 'erc-server "irc.libera.chat:6697")
(customize-set-variable 'erc-nick "aimebertrand")
(customize-set-variable 'erc-kill-buffer-on-part t)
(customize-set-variable 'erc-prompt-for-password nil)
(customize-set-variable 'erc-auto-query 'bury)
(customize-set-variable 'erc-track-shorten-start 8)


(provide 'timu-major-modes)

;;; timu-major-modes.el ends here
