;;; timu-func.el --- Custom global functions -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2020-11-08
;; Keywords: functions tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my created functions custom (helper) functions.

;;; Code:


(require 'dired)
(require 'eshell)
(require 'org)
(require 'org-capture)
(require 'project)


;;; DEFAULTS
(defgroup timu-func ()
  "Customise group for the `timu-func' Library."
  :group 'timu)

(defcustom timu-func-path
  (expand-file-name "libraries/timu-func.el" user-emacs-directory)
  "Variable for the path of the module `timu-func'."
  :type 'file
  :group 'timu-func)


;;; SETUP PACKAGE INSTALLATION
;; credit: https://github.com/bbatsov/prelude
(defun timu-func-packages-installed-p ()
  "Check if all packages in `timu-package-list' are installed."
  (cl-every #'package-installed-p timu-package-list))

(defun timu-func-require-package (package)
  "Install PACKAGE unless already installed."
  (unless (memq package timu-package-list)
    (add-to-list 'timu-package-list package))
  (unless (package-installed-p package)
    (package-install package)))

(defun timu-func-require-packages (packages)
  "Ensure PACKAGES are installed.
Missing packages are installed automatically."
  (mapc #'timu-func-require-package packages))

(defun timu-func-install-packages ()
  "Install all packages listed in `timu-package-list' unless already installed."
  (interactive)
  (unless (timu-func-packages-installed-p)
    ;; check for new packages (package versions)
    (message "%s" "Reloading packages DB...")
    (package-refresh-contents)
    (message "%s" " done.")
    ;; install the missing packages
    (timu-func-require-packages timu-package-list)))

(defun timu-func-require-modules ()
  "Load all custom the modules using `require'.
The order in which the modules are loaded is important."
  (interactive)
  (progn
    (require 'timu-defaults)
    (require 'timu-evil)
    (require 'timu-ui)
    (require 'timu-dired)
    (require 'timu-org)
    (require 'timu-nav)
    (require 'timu-minibuffer)
    (require 'timu-transients)
    (require 'timu-editor)
    (require 'timu-shell)
    (pcase system-type
      ((or'darwin 'gnu/linux)
       (require 'timu-git)))
    (pcase system-type
      ((or'darwin 'gnu/linux)
       (require 'timu-prog)))
    (require 'timu-major-modes)
    (require 'timu-keys)
    (require 'timu-scratch)))


;;; VISIT MY EMACS CONFIG
(defun timu-func-visit-emacs-init ()
  "Load `init.el' file into a buffer."
  (interactive)
  (find-file user-init-file))

(defun timu-func-visit-emacs-early-init ()
  "Load `early-init.el' file into a buffer."
  (interactive)
  (find-file early-init-file))


;;; CUSTOM LOAD THEME FUNCTION
(defun timu-func-load-theme ()
  "`load-theme' without confirmation and with completion.
Disables the `custom-enabled-themes' first to start with a blank canvas."
  (interactive)
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme
   (intern
    (completing-read "Load custom theme: "
                     (mapcar #'symbol-name
                             (custom-available-themes)))) t))


;;; LOAD FILES IF THEY EXIST CUSTOM FUNCTION
(defun timu-func-load-if-exists (file)
  "Load the elisp FILE only if it exists and is readable using `file-readable-p'.
Credit: https://github.com/zamansky/using-emacs/blob/master/myinit.org."
  (if (file-readable-p file)
      (load-file file)))


;;; WORKING WITH THE CURRENT BUFFER FILE
(defun timu-func-rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

(defun timu-func-delete-file-and-buffer ()
  "Delete the current file, and kill the buffer."
  (interactive)
  (unless (buffer-file-name)
    (error "No file is currently being edited"))
  (when (yes-or-no-p (format "Really delete '%s'?"
                             (file-name-nondirectory buffer-file-name)))
    (delete-file (buffer-file-name))
    (kill-this-buffer)))

(defun timu-func-move-buffer-file (dir)
  "Move both current buffer and file it's visiting to DIR."
  (interactive "DNew directory: ")
  (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (dir
          (if (string-match dir "\\(?:/\\|\\\\)$")
              (substring dir 0 -1) dir))
         (newname (concat dir "/" name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (progn
        (copy-file filename newname 1)
        (delete-file filename)
        (set-visited-file-name newname)
        (set-buffer-modified-p nil) t))))

(defun timu-func-open-current-file-with ()
  "Open current `buffer-file-name' with a macOS App.
This uses `completing-read' to select the App."
  (interactive)
  (let* ((apps-list
          (append (directory-files "/System/Applications" nil "\\.app$")
                  (directory-files "/Applications" nil "\\.app$")))
         (selected-app (completing-read "Choose an application: " apps-list)))
    (shell-command (format "open %s -a '%s'" (buffer-file-name) selected-app))))


;;; TEXT EDITING
;;;; inserting empty lines without moving
(defun timu-func-insert-line-before (lines)
  "Insert a newline above the line containing the cursor.
The number of LINES is controlled by the `prefix-arg'.
Credit: https://gist.github.com/onimenotsuki/adb1725ceddaeac026fc47bed30f64f6."
  (interactive "p")
  (save-excursion
    (move-beginning-of-line 1)
    (newline lines)))

(defun timu-func-insert-line-after (lines)
  "Insert a newline below the line containing the cursor.
The number of LINES is controlled by the `prefix-arg'.
Credit: https://gist.github.com/onimenotsuki/adb1725ceddaeac026fc47bed30f64f6."
  (interactive "p")
  (save-excursion
    (move-end-of-line 1)
    (open-line lines)))

;;;; transform to snake_case
(defun timu-func-snake-case (&optional txt)
  "Replace spaces in selected TXT into underscores."
  (interactive)
  (kmacro-exec-ring-item
   '([?\C-x ?r ?e ?p ?l ?a ?c ?e ?- ?r ?e ?g ?e ?x ?p
            return ?  return ?_ return] 0 "%d") txt))


;;; ORG-MODE FUNCTIONS
(defun timu-func-org-go-to-heading (&optional heading)
  "Go to an outline HEADING with `consult-org-heading'.
Also move the heading to the top of the buffer with `evil-scroll-line-to-top'"
  (interactive)
  (consult-org-heading)
  (evil-scroll-line-to-top heading))

(defun timu-func-outline-go-to-heading (&optional heading)
  "Go to an outline HEADING with `consult-outline'.
Also move the heading to the top of the buffer with `evil-scroll-line-to-top'"
  (interactive)
  (consult-outline)
  (evil-scroll-line-to-top heading))

(defun timu-func-outline-hide-sublevels ()
  "Go to the top of the file then `outline-hide-sublevels'.
This hides everything but the top LEVELS levels of headers, in whole buffer."
  (interactive)
  (goto-char (point-min))
  (call-interactively #'outline-hide-sublevels))

(defun timu-func-org-align-all-tags ()
  "Align the tags in all headings."
  (interactive)
  (org-align-all-tags))

(defun timu-func-org-tree-to-right ()
  "Version of `org-tree-to-indirect-buffer' that follows with the cursor."
  (interactive)
  (org-tree-to-indirect-buffer)
  (other-window 1))

(defun timu-func-org-toggle-emphasis-markers ()
  "Togle the visibility of the Org-mode Emphasis markers.
Requires `org-mode-restart' to take effect in the current session."
  (interactive)
  (if (equal org-hide-emphasis-markers t)
      (customize-set-variable 'org-hide-emphasis-markers nil)
    (customize-set-variable 'org-hide-emphasis-markers t))
  (org-mode-restart))

(defun timu-func-notes-header-clean (&optional arg)
  "Function to cleanup Notes captured with `timu-func-url-safari-capture-to-org'.
It is recorded from a Keyboard macro.
Optional argument ARG."
  (interactive "p")
  (kmacro-exec-ring-item
   '([121 121 down
          112 48 99 119 45 32 85 82 76 32 58 58
          escape up up 48 102 91 120 120 100 102 91 102 93 120 120
          escape down down 48 102 93 59 59 right 68]
     0 "%d") arg))


;;; WORKING WITH WINDOWS
(defun timu-func-split-below ()
  "Split the selected window in two with the new window is bellow.
This uses `split-window-below' but keep the cursor at position."
  (interactive)
  (split-window-below))

(defun timu-func-split-right ()
  "Split the selected window in two with the new window is to the right.
This uses `split-window-right' but keep the cursor at position."
  (interactive)
  (split-window-right))

(defun timu-func-split-and-follow-below ()
  "Split the selected window in two with the new window is bellow.
This uses `split-window-below' but follows with the cursor."
  (interactive)
  (split-window-below)
  (other-window 1))

(defun timu-func-split-and-follow-right ()
  "Split the selected window in two with the new window is to the right.
This uses `split-window-right' but follows with the cursor."
  (interactive)
  (split-window-right)
  (other-window 1))

(defun timu-func-find-file-below ()
  "Open file with `find-file' & `read-file-name' in a split window bellow."
  (interactive)
  (split-window-below)
  (other-window 1)
  (find-file (read-file-name "Find file: " nil) t))

(defun timu-func-find-file-right ()
  "Open file with `find-file' & `read-file-name' in a split window to the right."
  (interactive)
  (split-window-right)
  (other-window 1)
  (find-file (read-file-name "Find file: " nil) t))

(defun timu-func-split-bellow-all ()
  "Split the current buffer horizontally with new window bellow all other one.
The size ratio is 60 (top) to 40 (bottom).
Credit: https://emacs.stackexchange.com/a/60459/30874."
  (interactive)
  (split-window
   (frame-root-window)
   (truncate
    (* (window-total-height (frame-root-window)) 0.60)) 'below))

(defun timu-func-toggle-split-direction ()
  "Toggle window split from vertical to horizontal.
This work the other way around as well.
Credit: https://github.com/olivertaylor/dotfiles/blob/master/emacs/init.el"
  (interactive)
  (if (> (length (window-list)) 2)
      (error "Can't toggle with more than 2 windows")
    (let ((was-full-height (window-full-height-p)))
      (delete-other-windows)
      (if was-full-height
          (split-window-vertically)
        (split-window-horizontally))
      (save-selected-window
        (other-window 1)
        (switch-to-buffer (other-buffer))))))


;;; WORKING WITH BUFFERS
(defun timu-func-kill-current-buffer ()
  "Kill the current buffer using `kill-buffer'."
  (interactive)
  (kill-buffer (current-buffer)))

(defun timu-func-find-file-as-root ()
  "Like `find-file', but automatically edit the file with root-privileges.
This uses Tramp to apend sudo to path, if the file is not writable by user."
  (interactive)
  (let ((file (read-file-name "Edit as root: ")))
    (unless (file-writable-p file)
      (setq file (concat "/sudo:root@localhost:" file)))
    (find-file file)))

(defun timu-func-revert-buffer ()
  "Call `revert-buffer' with the NOCONFIRM argument set."
  (interactive)
  (revert-buffer nil t))


;;; WORKING WITH DATES
(defun timu-func-date-iso ()
  "Insert the current date, ISO format, e.g. `1996-12-26'."
  (interactive)
  (insert (format-time-string "%F")))

(defun timu-func-date-iso-with-time ()
  "Insert the current date, ISO format with time, e.g. `1996-12-26T14:30:30+0100'."
  (interactive)
  (insert (format-time-string "%FT%T%z")))

(defun timu-func-date-long ()
  "Insert the current date, long format, e.g. `December 26, 1996'."
  (interactive)
  (insert (format-time-string "%B %d, %Y")))

(defun timu-func-date-long-with-time ()
  "Insert the current date, long format, e.g. `December 26, 1996 - 14:30'."
  (interactive)
  (insert (capitalize (format-time-string "%B %d, %Y - %H:%M"))))

(defun timu-func-date-short ()
  "Insert the current date, short format, e.g. `26.12.1996'."
  (interactive)
  (insert (format-time-string "%d.%m.%Y")))

(defun timu-func-date-short-with-time ()
  "Insert the current date, short format with time, e.g. `1996.12.26 14:30'."
  (interactive)
  (insert (format-time-string "%d.%m.%Y %H:%M")))

(defun timu-func-insert-header-month ()
  "Insert a date as an org heading in the format `1970-01 January'."
  (interactive)
  (insert "** " (format-time-string "%Y-%m %B")))

(defun timu-func-insert-header-day ()
  "Insert a date as an org heading in the format `1970-01-01 Thursday'."
  (interactive)
  (insert "*** " (format-time-string "%Y-%m-%d %A")))

(defun timu-func-insert-org-datetree ()
  "Insert a datetree as org headings."
  (interactive)
  (insert "** " (format-time-string "%Y-%m %B") "\n"
          "*** " (format-time-string "%Y-%m-%d %A")))


;;; FIXING COMPANY COMPLETE TAB ISSUE
(defun timu-func-company-complete-selection ()
  "Insert the selected candidate or the first if none are selected."
  (interactive)
  (if company-selection
      (company-complete-selection)
    (company-complete-number 1)))


;;; FIND CONTENT AND FILES WITH DIR SELECTION AND COMPLETION
(defun timu-func-consult-rg ()
  "Function for `consult-ripgrep' with the `universal-argument'."
  (interactive)
  (consult-ripgrep (list 4)))

(defun timu-func-consult-fd ()
  "Function for `consult-find' with the `universal-argument'."
  (interactive)
  (consult-find (list 4)))


;;; EMBARK-ACT WITH UNIVERSAL-ARGUMENT
(defun timu-func-embark-act ()
  "Function for `emabark-act' with the `universal-argument'."
  (interactive)
  (embark-act (list 4)))


;;; FUNCTIONS FOR POPPER.EL
(defun timu-func-popper-toggle-all ()
  "Toggle all pupups at once with `popper.el'."
  (interactive)
  (popper-toggle-latest 16))


;;; CREATE SCRATCH BUFFER
(defun timu-func-get-buffer-create-scratch ()
  "Create a \"*scratch*\" buffer, if it does not exist.
This is like `startup--get-buffer-create-scratch', but interactive.
It also adds the custom `initial-scratch-message' at the top."
  (interactive)
  (or (get-buffer "*scratch*")
      (with-current-buffer (get-buffer-create "*scratch*")
        (set-buffer-major-mode (get-buffer "*scratch*"))
        (current-buffer)
        (goto-char (point-max))
        (insert initial-scratch-message)))
  (switch-to-buffer (get-buffer "*scratch*")))


;;; DIRED FUNCTIONS
(defun timu-func-dired-up-directory ()
  "Go up a directory in `dired'."
  (interactive)
  (find-alternate-file ".."))

(defun timu-func-async-shell-command-no-window (command)
  "Do not display the `async-shell-command' COMMAND output buffer.
Credit: https://stackoverflow.com/a/60333836
Credit: https://stackoverflow.com/a/47910509."
  (interactive)
  (let ((display-buffer-alist
         (list (cons
                "\\*Async Shell Command\\*.*"
                (cons #'display-buffer-no-window nil)))))
    (async-shell-command command)))

(defun timu-func-dired-find-marked-file (&optional arg)
  "Open each of the marked files, or the file under the point.
When prefix arg, the next N files "
  (interactive "P")
  (let* ((fn-list (dired-get-marked-files nil arg)))
    (mapc 'find-file fn-list)))

(defun timu-func-dired-shell-open ()
  "Open the files at point with shell command \"open\".
This will open the file at point with the default App in macOS."
  (interactive)
  (setq file (dired-get-file-for-visit))
  (timu-func-async-shell-command-no-window (concat "open " (shell-quote-argument file))))

(defun timu-func-dired-shell-open-dir ()
  "Open current directory at point with shell command \"open\".
This will open \"Finder.app\" at current location."
  (interactive)
  (timu-func-async-shell-command-no-window "open ./" ))

(defun timu-func-dired-shell-quicklook ()
  "Open the files at point with shell command \"qlmanage\".
This will display a Quicklook of the file at point in macOS."
  (interactive)
  (setq file (dired-get-file-for-visit))
  (timu-func-async-shell-command-no-window
   (concat "qlmanage -p " (shell-quote-argument file) " > /dev/null 2>&1")))

(defun timu-func-dired-copy-path-at-point ()
  "Copy the full path of the at `point' to the `kill-ring'.
Credit: https://emacs.stackexchange.com/a/36851/30874"
  (interactive)
  (dired-copy-filename-as-kill 0))

(unless (display-graphic-p)
  (progn
    (add-hook 'dired-mode-hook (lambda () (evil-leader/set-leader "\\")))))

(defun timu-func-open-all-in-emacs (&optional file)
  "Open the current FILE or Dired marked files in Emacs."
  (interactive)
  (let (doIt (myFileList
              (cond
               ((eq major-mode 'dired-mode)
                (dired-get-marked-files))
               ((not file) (list (buffer-file-name)))
               (file (list file)))))
    (setq doIt (if (<= (length myFileList) 30) t
                 (y-or-n-p "Open more than 30 files? ")))
    (mapc (lambda (fPath)
            (let ((process-connection-type nil))
              (start-process "" nil "emacsclient" fPath))) myFileList)))

(defun timu-func-open-in-external-app (&optional file)
  "Open the current FILE or Dired marked files in external app.
The app is chosen from your OS's preference.
Credit: http://xahlee.info/emacs/emacs/emacs_dired_open_file_in_ext_apps.html."
  (interactive)
  (let (doIt (myFileList
              (cond
               ((eq major-mode 'dired-mode)
                (dired-get-marked-files))
               ((not file) (list (buffer-file-name)))
               (file (list file)))))
    (setq doIt (if (<= (length myFileList) 30) t (y-or-n-p "Open more than 30 files? ")))
    (when doIt
      (cond
       ((string-equal system-type "windows-nt")
        (mapc (lambda (fPath)
                (w32-shell-execute "open" (replace-regexp-in-string "/" "\\" fPath t t)))
              myFileList))
       ((string-equal system-type "darwin")
        (mapc (lambda (fPath)
                (shell-command (format "open \"%s\"" fPath)))
              myFileList))
       ((string-equal system-type "gnu/linux")
        (mapc (lambda (fPath)
                (let ((process-connection-type nil))
                  (start-process "" nil "xdg-open" fPath))) myFileList))))))

(defun timu-func-dired-search-and-enter ()
  "Search file or directory with `consult-line' and then visit it."
  (interactive)
  (consult-line)
  (dired-find-alternate-file))


;;; ESHELL
(defun timu-func-eshell-handle-ansi-color ()
  (ansi-color-apply-on-region eshell-last-output-start
                              eshell-last-output-end))

(defun timu-func-with-face (str &rest face-plist)
  (propertize str 'face face-plist))

(defun timu-func-custom-eshell-prompt ()
  (let* ((git-branch-unparsed
          (shell-command-to-string
           "git rev-parse --abbrev-ref HEAD 2>/dev/null")) ;; get git branch.
         (git-branch
          (if (string= git-branch-unparsed "") ""

            (substring git-branch-unparsed 0 -1)))) ;; remove trailing newline.
    (concat (unless (string= git-branch "")
              (timu-func-with-face
               (concat git-branch " ")
               :inherit font-lock-constant-face)) ;; git branch.
            (timu-func-with-face
             (concat (car (last (split-string (eshell/pwd) "/"))) " ")
             :inherit font-lock-builtin-face) ;; directory.
            ;; Prompt.
            ;; NOTE: Need to keep " $" for the next/previous prompt regexp to work.
            (timu-func-with-face "$" :inherit font-lock-preprocessor-face) " ")))


;;; ORG CAPTURE FUNCTIONS
(defun timu-func-make-capture-frame ()
  "Create a new frame and run `org-capture'."
  (interactive)
  (make-frame '((name . "capture")
                (top . 300)
                (left . 700)
                (width . 80)
                (height . 25)))
  (select-frame-by-name "capture")
  (delete-other-windows)
  (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
    (org-capture)))


;;; ORG REFILING SUBTREE TO A STANDALONE FILE
(defun timu-func-org-get-subtree-tags (&optional props)
  "Given PROPS, from a call to `org-entry-properties', return a list of tags."
  (unless props
    (setq props (org-entry-properties)))
  (let ((tag-label (if timu-func-org-get-subtree-tags-inherited "ALLTAGS" "TAGS")))
    (-some->> props
      (assoc tag-label)
      cdr
      substring-no-properties
      (s-split ":")
      (--filter (not (cl-equalp "" it))))))

(defun timu-func-org-get-subtree-properties (attributes)
  "Return a list of tuples of a subtrees ATTRIBUTES where the keys are strings."

  (defun timu-func-symbol-upcase-p (sym)
    (let ((case-fold-search nil))
      (string-match-p "^:[A-Z]+$" (symbol-name sym))))

  (defun timu-func-convert-tuple (tup)
    (let ((key (cl-first tup))
          (val (cl-second tup)))
      (list (substring (symbol-name key) 1) val)))

  (->> attributes
       (-partition 2) ; Convert plist to list of tuples
       (--filter (timu-func-symbol-upcase-p (cl-first it))) ; Remove lowercase tuples
       (-map 'timu-func-convert-tuple)))

(defun timu-func-org-get-subtree-content (attributes)
  "Return the contents and ATTRIBUTES of the current subtree as a string."
  (let ((header-components '(clock diary-sexp drawer headline inlinetask
                                   node-property planning property-drawer section)))
    (goto-char (plist-get attributes :contents-begin))
    ;; Walk down past the properties, etc.
    (while
        (let* ((cntx (org-element-context))
               (elem (cl-first cntx))
               (props (cl-second cntx)))
          (when (member elem header-components)
            (goto-char (plist-get props :end)))))
    ;; At this point, we are at the beginning of what we consider
    ;; the contents of the subtree, so we can return part of the buffer:
    (buffer-substring-no-properties (point) (org-end-of-subtree))))

(defun timu-func-org-subtree-metadata ()
  "Return a list of key aspects of an org-subtree.
Includes the following: header text, body contents, list of tags,
region list of the start and end of the subtree."
  (save-excursion
    ;; Jump to the parent header if not already on a header
    (when (not (org-at-heading-p))
      (org-previous-visible-heading 1))
    (let* ((context (org-element-context))
           (attrs   (cl-second context))
           (props   (org-entry-properties)))
      (list :region     (list (plist-get attrs :begin) (plist-get attrs :end))
            :header     (plist-get attrs :title)
            :tags       (timu-func-org-get-subtree-tags props)
            :properties (timu-func-org-get-subtree-properties attrs)
            :body       (timu-func-org-get-subtree-content attrs)))))

(defvar timu-func-org-get-subtree-tags-inherited t
  "Returns a subtree's tags, and all tags inherited.
This is from tags specified in parents headlines or on the file itself.
Defaults to true.")

(defun timu-func-org-set-file-property (key value &optional spot)
  "Make sure file has a top-level, file-wide property.
KEY is something like \"TITLE\" or \"FILETAGS\".
This function makes sure that the property contains the contents of VALUE,
and if the file doesn't have the property, it is inserted at either SPOT,
or if nil,the top of the file."
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search t))
      (if (re-search-forward (format "^#\\+%s:\s*\\(.*\\)" key) nil t)
          (replace-match value nil nil nil 1)
        (cond
         ;; if SPOT is a number, go to it:
         ((numberp spot) (goto-char spot))
         ;; If SPOT is not given, jump to first blank line:
         ((null spot) (progn (goto-char (point-min))
                             (re-search-forward "^\s*$" nil t)))
         (t (goto-char (point-min))))
        (insert (format "#+%s: %s\n" (upcase key) value))))))

(defun timu-func-org-create-org-file (filepath header body tags properties)
  "Create a new Org file by FILEPATH.
The file content is pre-populated with the HEADER,
BODY, any associated TAGS and PROPERTIES."
  (find-file-other-window filepath)
  (timu-func-org-set-file-property "TITLE" header t)
  (when tags
    (timu-func-org-set-file-property "FILETAGS" (s-join " " tags)))
  ;; Insert any drawer properties as #+PROPERTY entries:
  (when properties
    (goto-char (point-min))
    (or (re-search-forward "^\s*$" nil t) (point-max))
    (--map (insert (format "#+PROPERTY: %s %s" (cl-first it) (cl-second it))) properties))
  ;; My auto-insert often adds an initial headline for a subtree, and in this
  ;; case, I don't want that... Yeah, this isn't really globally applicable,
  ;; but it shouldn't cause a problem for others.
  (when (re-search-forward "^\\* [0-9]$" nil t)
    (replace-match ""))
  (delete-blank-lines)
  (goto-char (point-max))
  (insert "\n")
  (insert body))

(defun timu-func-org-filename-from-title (title)
  "Create a useful filename based on a header string, TITLE.
For instance, given the string: \"What's all this then?\"
This function will return: `whats-all-this-then'."
  (let* ((no-letters (rx (one-or-more (not alphanumeric))))
         (init-try (->> title
                        downcase
                        (replace-regexp-in-string "'" "")
                        (replace-regexp-in-string no-letters "-"))))
    (string-trim init-try "-+" "-+")))

(defun timu-func-org-refile-subtree-to-file (dir)
  "Archive the `org-mode' subtree and create an entry in the directory DIR.
It attempts to move as many of the properties and features to the new file."
  (interactive "DESTINATION: ")
  (let* ((props      (timu-func-org-subtree-metadata))
         (head       (plist-get props :header))
         (body       (plist-get props :body))
         (tags       (plist-get props :tags))
         (properties (plist-get props :properties))
         (area       (plist-get props :region))
         (filename   (timu-func-org-filename-from-title head))
         (filepath   (format "%s/%s.org" dir filename)))
    (apply #'delete-region area)
    (timu-func-org-create-org-file filepath head body tags properties)))

(defvar timu-func-org-refile-directly-show-after nil
  "Show the destination afterwards when using `timu-func-org-refile-directly'.
This is if this is set to t, otherwise, just do all in the background.")

(defun timu-func-org-subtree-region ()
  "Return a list of the start and end of a subtree."
  (save-excursion
    (list (progn (org-back-to-heading) (point))
          (progn (org-end-of-subtree)  (point)))))

(defun timu-func-org-refile-directly (file-dest)
  "Move the current subtree to the end of FILE-DEST.
If SHOW-AFTER is non-nil, show the destination window,
otherwise, this destination buffer is not shown."
  (interactive "fDestination: ")

  (defun timu-func-dump-it (file contents)
    (find-file-other-window file-dest)
    (goto-char (point-max))
    (insert "\n" contents))

  (save-excursion
    (let* ((region (timu-func-org-subtree-region))
           (contents (buffer-substring (cl-first region) (cl-second region))))
      (apply #'kill-region region)
      (if timu-func-org-refile-directly-show-after
          (save-current-buffer (timu-func-dump-it file-dest contents))
        (save-window-excursion (timu-func-dump-it file-dest contents))))))


;;; PROJECT FUNCTIONS
(defun timu-func-project-switch-project (dir)
  "\"Switch\" to another project by running an Emacs command.
Directly use `project-find-file' instead of getting prompted.

When called in a program, it will use the project corresponding
to directory DIR."
  (interactive (list (project-prompt-project-dir)))
  (let ((default-directory dir)
        (project-current-inhibit-prompt t))
    (project-find-file)))


;;; OTHER PDF FUNCTIONS
(defun timu-func-pdf-password-protect ()
  "Password protect current pdf in buffer or `dired' file.
Needs the cli tool `qpdf' installed with `brew install qpdf'.
Credit: https://xenodium.com/emacs-password-protect-current-pdf/"
  (interactive)
  (unless (executable-find "qpdf")
    (user-error "Program qpdf not installed"))
  (unless
      (equal "pdf"
             (or
              (when (buffer-file-name)
                (downcase (file-name-extension (buffer-file-name))))
              (when (dired-get-filename nil t)
                (downcase (file-name-extension (dired-get-filename nil t))))))
    (user-error "No pdf to act on"))
  (let* ((user-password (read-passwd "user-password: "))
         (owner-password (read-passwd "owner-password: "))
         (input (or (buffer-file-name)
                    (dired-get-filename nil t)))
         (output (concat (file-name-sans-extension input)
                         "_enc.pdf")))
    (message
     (string-trim
      (shell-command-to-string
       (format "qpdf --verbose --encrypt '%s' '%s' 256 -- '%s' '%s'"
               user-password owner-password input output))))))


;;; MACRO FOR SETTING VARIABLES
(defmacro timu-func-setv (&rest args)
  "Handle ARGS like `setq' using `customize-set-variable'.
Credit https://www.reddit.com/r/emacs/comments/qg8tga/comment/hi4xp8d."
  (let (body)
    (while args
      (let* ((var (pop args)) (val (pop args)))
        (push `(customize-set-variable ',var ,val) body)))
    (macroexp-progn (nreverse body))))


;;; TAB-BAR-MODE
(defun timu-func-tab-bar-new-tab ()
  "Create a new tab an then switch to the scratch buffer.
If it has been closed, then create one."
  (interactive)
  (tab-bar-new-tab)
  (timu-func-get-buffer-create-scratch))


;;; MODELINE
(defun timu-func-flash-mode-line ()
  "Flash the modeline on error or warning instead of the bell."
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil #'invert-face 'mode-line))


;;; SWAP CTRL & ALT ON WINDOWS
;; Credit: https://gist.github.com/mmarshall540/8db9b5bac8dc5670cb9323e387de1317
;; There seems to be no built-in mechanism to swap modifier keys in
;; Emacs, but it can be accomplished (for the most part) by
;; translating a near-exhaustive list of modifiable keys.  In the case
;; of 'control and 'meta, some keys must be omitted to avoid errors or
;; other undesired effects.
;;(pcase system-type
;;  ((or'windows-nt 'gnu/linux)
;;   (defun timu-func-make-key-string (modsymbol basic-event)
;;     "Convert the combination of MODSYMBOL and BASIC-EVENT.
;;BASIC-EVENT can be a character or a function-key symbol.  The
;;return value can be used with `define-key'."
;;     (vector (event-convert-list `(,modsymbol ,basic-event))))
;;
;;   ;; Escaped chars are:
;;   ;; tab return space del backspace (typically translated to del)
;;   (dolist
;;       (char
;;        (append
;;         '(up down left right menu print scroll pause
;;              insert delete home end prior next
;;              tab return space backspace escape
;;              f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12)
;;         ;; Escape gets translated to `C-\[' in `local-function-key-map'
;;         ;; We want that to keep working, so we don't swap `C-\[' with `M-\['.
;;         (remq ?\[ (number-sequence 33 126))))
;;     ;; Changing this to use `input-decode-map', as it works for more keys.
;;     (define-key input-decode-map
;;       (timu-func-make-key-string 'control char)
;;       (timu-func-make-key-string 'meta char))
;;     (define-key input-decode-map
;;       (timu-func-make-key-string 'meta char)
;;       (timu-func-make-key-string 'control char)))))


(provide 'timu-func)

;;; timu-func.el ends here
