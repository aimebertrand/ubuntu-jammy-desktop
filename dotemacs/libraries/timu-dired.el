;;; timu-dired.el --- Dired file manager config -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: tools filemanagement dired
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the customization of Dired.

;;; Code:


(require 'wdired)
(require 'dired-filetype-face)
(require 'treemacs-all-the-icons)
(require 'treemacs-icons-dired)


;;; DEFAULTS
(defgroup timu-dired ()
  "Customise group for the `timu-dired' Library."
  :group 'timu)

(defcustom timu-dired-path
  (expand-file-name "libraries/timu-dired.el" user-emacs-directory)
  "Variable for the path of the module `timu-dired'."
  :type 'file
  :group 'timu-dired)


;;; DIRED CONFIG
(customize-set-variable 'dired-recursive-copies 'always)
(customize-set-variable 'dired-isearch-filenames 'dwim)
(customize-set-variable 'dired-listing-switches "-Ahlpo")
(customize-set-variable 'dired-dwim-target t)
(setq insert-directory-program "ls")
(put 'dired-find-alternate-file 'disabled nil)
(add-hook 'after-init-hook
          (lambda ()
            (progn
              (treemacs-load-theme "all-the-icons"))))

;; allow editing file permissions with wdired
(customize-set-variable 'wdired-allow-to-change-permissions t)
(customize-set-variable 'wdired-create-parent-directories t)

(add-hook 'dired-mode-hook #'hl-line-mode)
(add-hook 'dired-mode-hook #'dired-hide-details-mode)

(dired-async-mode 1)
(turn-on-gnus-dired-mode)

(when (string= system-type "darwin")
  (customize-set-variable 'dired-use-ls-dired nil)) ;; can't use ls on macOS


;;; DIRED-ICONS
(add-hook 'dired-mode-hook #'treemacs-icons-dired-mode)


;;; DIRED KEYBINDINGS
(evil-define-key 'normal dired-mode-map (kbd "/") #'dired-narrow)
(evil-define-key 'normal dired-mode-map (kbd "G") #'evil-goto-line)
(evil-define-key 'normal dired-mode-map (kbd "g g") #'evil-goto-first-line)
(evil-define-key 'normal dired-mode-map (kbd "v") #'evil-visual-char)
(evil-define-key 'normal dired-mode-map (kbd "<tab>") #'dired-subtree-cycle)

(define-key wdired-mode-map (kbd "M-s") #'wdired-finish-edit)
(define-key wdired-mode-map (kbd "M-w") #'wdired-abort-changes)

(define-key dired-mode-map (kbd "M-c") 'timu-func-dired-copy-path-at-point)
(define-key dired-mode-map (kbd "M-ƒ") #'timu-func-dired-search-and-enter)
(define-key dired-mode-map (kbd "M-f") #'consult-line)
(define-key dired-mode-map (kbd "M-<up>") #'timu-func-dired-up-directory)
(define-key dired-mode-map [remap dired-next-line] 'timu-func-dired-shell-quicklook)
(define-key dired-mode-map [remap dired-sort-toggle-or-edit] 'timu-func-open-in-external-app)
(define-key dired-mode-map [remap dired-prev-subdir] 'popper-toggle-latest)
(define-key dired-mode-map [remap dired-find-file] #'dired-find-alternate-file)

(pcase system-type
  ('darwin
   (define-key dired-mode-map (kbd "M-∂") #'timu-func-dired-shell-open-dir))
  ((or 'gnu/linux 'windows-nt)
   (define-key dired-mode-map (kbd "M-s-d") #'timu-func-dired-shell-open-dir)))

;;; DIRED-FILETYPE-FACE
(deffiletype-face-regexp omit3
  :type-for-docstring hidden :regexp "^XXXXX")


(provide 'timu-dired)

;;; timu-dired.el ends here
