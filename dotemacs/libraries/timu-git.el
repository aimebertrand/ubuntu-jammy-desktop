;;; timu-git.el --- Configuration for magit & git in general -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: git magit tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my config for git and magit.

;;; Code:


(require 'magit)
(require 'with-editor)


;;; DEFAULTS
(defgroup timu-git ()
  "Customise group for the `timu-git' Library."
  :group 'timu)

(defcustom timu-git-path
  (expand-file-name "libraries/timu-git.el" user-emacs-directory)
  "Variable for the path of the module `timu-git'."
  :type 'file
  :group 'timu-git)


;;; MAGIT
(customize-set-variable 'git-commit-summary-max-length 50)

;; enter commit mode in evil insert state
(add-hook 'git-commit-mode-hook #'evil-insert-state)
(add-hook 'git-commit-mode-hook #'git-commit-turn-on-flyspell)


;;; EDIFF
(customize-set-variable 'ediff-split-window-function #'split-window-horizontally)
(customize-set-variable 'ediff-window-setup-function #'ediff-setup-windows-plain)
(customize-set-variable 'ediff-diff-options "-w")
(customize-set-variable 'ediff-use-long-help-message t)

;;; GIT GUTTER PLUS
(global-git-gutter+-mode)

;; should fix opening file issue with tramp
;; https://github.com/syl20bnr/spacemacs/issues/12860
(remove-hook 'find-file-hook #'git-gutter+-turn-on)


;;; GIT-MODES
(add-to-list 'auto-mode-alist '("\\.gitattributes\\'" . gitattributes-mode))
(add-to-list 'auto-mode-alist '("\\gitattributes\\'" . gitattributes-mode))

(add-to-list 'auto-mode-alist '("\\.gitconfig\\'" . gitconfig-mode))
(add-to-list 'auto-mode-alist '("\\gitconfig\\'" . gitconfig-mode))

(add-to-list 'auto-mode-alist '("\\.gitignore\\'" . gitignore-mode))
(add-to-list 'auto-mode-alist '("\\gitignore\\'" . gitignore-mode))


;;; WITH-EDITOR KEYBINDINGS
(define-key with-editor-mode-map (kbd "M-i") #'timu-func-insert-silly-commit-message)
(define-key with-editor-mode-map (kbd "M-s") #'with-editor-finish)
(define-key with-editor-mode-map (kbd "M-w") #'with-editor-cancel)


(provide 'timu-git)

;;; timu-git.el ends here
