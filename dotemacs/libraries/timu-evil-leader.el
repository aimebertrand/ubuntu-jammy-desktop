;;; timu-evil-leader.el --- Custom evil-leader keybindings -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2021-12-28
;; Keywords: tools keybindings evil leader
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my keybindings used with `evil-leader'.

;;; Code:


(require 'evil-leader)


;;; DEFAULTS
(defgroup timu-evil-leader ()
  "Customise group for the `timu-evil-leader' Library."
  :group 'timu)

(defcustom timu-evil-leader-path
  (expand-file-name "libraries/timu-evil-leader.el" user-emacs-directory)
  "Variable for the path of the module `timu-evil-leader'."
  :type 'file
  :group 'timu-evil-leader)


;;; SETUP
(global-evil-leader-mode)

(evil-leader/set-leader "<SPC>")


;;; GENERAL STUFF
(evil-leader/set-key "B" 'ibuffer) ; (ibuffer; SPC-shift-b)
(evil-leader/set-key "o" 'find-file)
(evil-leader/set-key "O" 'timu-func-find-file-as-root)
(evil-leader/set-key "p" 'timu-func-project-switch-project)
(evil-leader/set-key "S" 'write-file) ; (save as; SPC-shift-s)
(evil-leader/set-key "c" 'kill-ring-save)
(evil-leader/set-key "g" 'magit) ; (SPC-alt-g)
(evil-leader/set-key "n" 'evil-buffer-new)
(evil-leader/set-key "q" 'save-buffers-kill-terminal)
(evil-leader/set-key "s" 'save-buffer)
(evil-leader/set-key "t s" 'save-some-buffers)
(evil-leader/set-key "v" 'yank)
(evil-leader/set-key "w" 'kill-current-buffer)
(evil-leader/set-key "x" 'execute-extended-command)
(evil-leader/set-key "∫" 'bookmark-jump) ; (SPC-alt-b)
(evil-leader/set-key "π" 'project-find-file); (SPC-alt-p)
(evil-leader/set-key "∑" 'delete-frame) ; (SPC-alt-w)


;;; HANDLING MY CONFIG
(evil-leader/set-key "t l" 'timu-func-load-theme)
(evil-leader/set-key "t e" 'timu-func-config-edit)
(evil-leader/set-key "t r" 'timu-func-config-reload)
(evil-leader/set-key "€" 'timu-func-config-edit)
(evil-leader/set-key "®" 'timu-func-config-reload)


;;; WINDOWS
;;;; window manipulation
(evil-leader/set-key "‚" 'ace-swap-window)
(evil-leader/set-key "2" 'ace-delete-window)
(evil-leader/set-key "1" 'ace-delete-other-window)
(evil-leader/set-key "0" 'delete-window)
(evil-leader/set-key "≠" 'balance-windows) ; (SPC-alt-0)
(evil-leader/set-key "t n" 'make-frame-command)

;;;; window spliting
(evil-leader/set-key "ö" 'split-and-follow-horizontally)
(evil-leader/set-key "ä" 'split-and-follow-vertically)
(evil-leader/set-key "æ" 'timu-func-toggle-split-direction) ; (SPC-alt-ä)

;;;; window resizing
(evil-leader/set-key "…" 'evil-window-increase-width)
(evil-leader/set-key "∞" 'evil-window-decrease-width)
(evil-leader/set-key "±" 'evil-window-increase-height)
(evil-leader/set-key "–" 'evil-window-decrease-height)


;;; ORG-MODE
(evil-leader/set-key "ç" 'org-capture) ; (SPC-alt-c)
(evil-leader/set-key "å" 'org-archive-subtree) ; (SPC-alt-a)
(evil-leader/set-key "r" 'timu-func-org-refile-subtree-to-file)
(evil-leader/set-key "R" 'org-revert-all-org-buffers)


;;; VERTICO & CONSULT
(evil-leader/set-key "b" 'consult-buffer)
(evil-leader/set-key "f" 'consult-line)
(evil-leader/set-key-for-mode 'org-mode-map "ª" 'timu-func-outline-go-to-heading) ; (SPC-alt-h)
(evil-leader/set-key-for-mode 'emacs-lisp-mode-map "ª" 'timu-func-outline-go-to-heading) ; (SPC-alt-h)


;;; FINDING STUFF
(evil-leader/set-key "t f o" 'timu-func-search-org-files)
(evil-leader/set-key "t f p" 'timu-func-search-project-files)


;;; LSP-MODE
;;;; lsp server stuff
(evil-leader/set-key "l d" 'lsp-disconnect)
(evil-leader/set-key "l i" 'lsp-install-server)
(evil-leader/set-key "l l" 'lsp)
(evil-leader/set-key "l r" 'lsp-restart-workspace)
(evil-leader/set-key "l s" 'lsp-shutdown-workspace)

;;;; finding stuff with lsp
(evil-leader/set-key "l f c" 'lsp-find-declaration)
(evil-leader/set-key "l f d" 'lsp-find-definition)
(evil-leader/set-key "l f i" 'lsp-find-implementation)
(evil-leader/set-key "l f l" 'lsp-find-locations)
(evil-leader/set-key "l f r" 'lsp-find-references)


(provide 'timu-evil-leader)

;;; timu-evil-leader.el ends here
