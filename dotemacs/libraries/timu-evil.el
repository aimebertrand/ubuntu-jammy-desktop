;;; timu-evil.el --- Custom evil keybindings config -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: tools keybindings evil
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides custom settings the `evil' packages.

;;; Code:


(require 'evil)
(require 'evil-collection)


;;; DEFAULTS
(defgroup timu-evil ()
  "Customise group for the `timu-evil' Library."
  :group 'timu)

(defcustom timu-evil-path
  (expand-file-name "libraries/timu-evil.el" user-emacs-directory)
  "Variable for the path of the module `timu-evil'."
  :type 'file
  :group 'timu-evil)


;;; LOAD AND ACTIVATE EVIL
(evil-mode 1)

(evil-set-undo-system 'undo-tree)


;;; EVIL-COLLECTION
(evil-collection-init)


;;; EVIL-SURROUND
(global-evil-surround-mode 1)


(provide 'timu-evil)

;;; timu-evil.el ends here
