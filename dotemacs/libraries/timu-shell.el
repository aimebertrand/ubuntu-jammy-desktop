;;; timu-shell.el --- Config for many shells -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper shell
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for several Emacs shells.

;;; Code:


(require 'term)


;;; DEFAULTS
(defgroup timu-shell ()
  "Customise group for the `timu-shell' Library."
  :group 'timu)

(defcustom timu-shell-path
  (expand-file-name "libraries/timu-shell.el" user-emacs-directory)
  "Variable for the path of the module `timu-shell'."
  :type 'file
  :group 'timu-shell)


;;; SHELL
;; credit: https://www.reddit.com/r/emacs/comments/u9xv1m/amazing_in_native_windows_11s_emacs281_to_get
(pcase system-type
  ('windows-nt
   (customize-set-variable 'shell-file-name "C:/Windows/system32/bash.exe")
   (customize-set-variable 'sh-shell-file "C:/Windows/system32/bash.exe")
   (setenv "ESHELL" "bash")))


;;; ESHELL
(customize-set-variable
 'eshell-aliases-file
 (expand-file-name "local/eshell-aliases" user-emacs-directory))

(customize-set-variable
 'eshell-directory-name
 (expand-file-name "local/eshell/" user-emacs-directory))

(add-to-list 'eshell-output-filter-functions 'timu-func-eshell-handle-ansi-color)

(customize-set-variable 'eshell-prompt-function 'timu-func-custom-eshell-prompt)


;;; TERM
(pcase system-type
  ((or 'darwin 'gnu/linux)
   ;; term keybindings
   (define-key term-raw-map (kbd "M-o") #'find-files)
   (define-key term-raw-map (kbd "M-t") #'tab-bar-switch-to-next-tab)
   (define-key term-raw-map (kbd "M-q") #'save-buffers-kill-terminal)
   (define-key term-raw-map (kbd "M-<left>") #'evil-window-left)
   (define-key term-raw-map (kbd "M-<right>") #'evil-window-right)
   (define-key term-raw-map (kbd "M-<up>") #'evil-window-up)
   (define-key term-raw-map (kbd "M-<down>") #'evil-window-down)
   (define-key term-mode-map (kbd "C-M-p") #'popper-toggle-latest)))


;;; WORKING IN THE TTY
;;;; arrow keys for tty
;; This solves the issue of the arrow keys beeing sent as the escape codes in tty & co.
;; credit:
;; - http://kb.mit.edu/confluence/display/istcontrib/How+to+get+the+Arrow+keys+to+work+in+Emacs
;; - https://emacs.stackexchange.com/questions/7151/is-there-a-way-to-detect-that-emacs-is-running-in-a-terminal
(unless (display-graphic-p)
  (progn
    (defvar timu-shell-arrow-keys-map (make-sparse-keymap) "Keymap for arrow keys")
    (define-key esc-map "O" timu-shell-arrow-keys-map)
    (define-key timu-shell-arrow-keys-map "A" 'previous-line)
    (define-key timu-shell-arrow-keys-map "B" 'next-line)
    (define-key timu-shell-arrow-keys-map "C" 'forward-char)
    (define-key timu-shell-arrow-keys-map "D" 'backward-char)))

;;;; cursor shape for tty
;; credit: https://github.com/7696122/evil-terminal-cursor-changer/issues/19
(setq evil-motion-state-cursor 'box)  ; █
(setq evil-visual-state-cursor 'box)  ; █
(setq evil-normal-state-cursor 'box)  ; █
(setq evil-insert-state-cursor 'bar)  ; ⎸
(setq evil-emacs-state-cursor  'box) ; █

(unless (display-graphic-p)
  (etcc-on))


(provide 'timu-shell)

;;; timu-shell.el ends here
