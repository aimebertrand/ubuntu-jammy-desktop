;;; timu-keys.el --- Custom keybindings -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-02-14
;; Keywords: map tools helper keymap keybindings
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides custom bindings including a cutom map-prefix `C-t'.

;;; Code:


;;; DEFAULTS
(defgroup timu-keys ()
  "Customise group for the `timu-keys' Library."
  :group 'timu)

(defcustom timu-keys-path
  (expand-file-name "libraries/timu-keys.el" user-emacs-directory)
  "Variable for the path of the module `timu-keys'."
  :type 'file
  :group 'timu-keys)

;;; OS CONFIGURATION
(pcase system-type
  ;; macOS keys
  ('darwin
   ;; keyboard modifiers
   (customize-set-variable 'mac-command-modifier 'meta) ; make cmd key do Meta
   ;; emacs-plus or emacsmacport:
   (if
       (custom-variable-p 'mac-effective-appearance-change-hook)
       (customize-set-variable 'mac-function-modifier 'super)
     (customize-set-variable 'mac-option-modifier 'none))
   (customize-set-variable 'mac-control-modifier 'control) ; make Control key do Control
   (customize-set-variable 'ns-function-modifier 'super)
   (customize-set-variable 'ns-pop-up-frames nil))
  ;; windows keys
  ('windows-nt
   (setq w32-pass-lwindow-to-system nil)
   (setq w32-lwindow-modifier 'super))) ; Left Windows key


;;; GLOBAL KEYBINDINGS
;;;; global commands & functions
(global-set-key (kbd "M-B") #'ibuffer)
(global-set-key (kbd "M-g") #'magit-status)
(global-set-key (kbd "M-d") #'dired)
(global-set-key (kbd "M-,") #'timu-func-visit-emacs-init)
(global-set-key (kbd "M-;") #'timu-func-visit-emacs-early-init)
(global-set-key (kbd "M-v") #'yank)
(global-set-key (kbd "M-c") #'kill-ring-save)
(global-set-key (kbd "M-e") #'embark-act)
(global-set-key (kbd "C-M-+") #'timu-func-expenses-add-expense)
(global-set-key [remap describe-syntax] #'shortdoc-display-group)

(pcase system-type
  ('darwin
   (global-set-key (kbd "M-∂") #'docker)
   (global-set-key (kbd "C-M-†") #'timu-func-load-theme)
   (global-set-key (kbd "M-€") #'embark-dwim)
   (global-set-key (kbd "M-®") #'timu-func-revert-buffer)
   (global-set-key (kbd "M-«") #'read-only-mode)
   (global-set-key (kbd "∂") #'osx-dictionary-search-pointer))
  ('gnu/linux
   (global-set-key (kbd "C-M-s-t") #'timu-func-load-theme)
   (global-set-key (kbd "C-s-e") #'embark-dwim)
   (global-set-key (kbd "C-s-r") #'timu-func-revert-buffer)
   (global-set-key (kbd "C-s-q") #'read-only-mode))
  ('windows-nt
   (global-set-key (kbd "C-M-s-t") #'timu-func-load-theme)
   (global-set-key (kbd "C-s-e") #'embark-dwim)
   (global-set-key (kbd "C-s-r") #'timu-func-revert-buffer)
   (global-set-key (kbd "C-s-q") #'read-only-mode)))

;;;; frames & windows
(global-unset-key (kbd "M-<left>"))
(global-unset-key (kbd "M-<right>"))
(global-unset-key (kbd "M-<up>"))
(global-unset-key (kbd "M-<down>"))

(global-set-key (kbd "M-<left>") #'windmove-left)
(global-set-key (kbd "M-<right>") #'windmove-right)
(global-set-key (kbd "M-<up>") #'windmove-up)
(global-set-key (kbd "M-<down>") #'windmove-down)

(global-set-key (kbd "M-2") #'ace-delete-window)
(global-set-key (kbd "M-N") #'make-frame-command)
(global-set-key (kbd "M-0") #'delete-window)
(global-set-key (kbd "M-1") #'delete-other-windows)

(pcase system-type
  ('darwin
   (global-set-key (kbd "M-…") #'enlarge-window-horizontally)
   (global-set-key (kbd "M-∞") #'shrink-window-horizontally)
   (global-set-key (kbd "M-±") #'enlarge-window)
   (global-set-key (kbd "M-–") #'shrink-window))
  ('gnu/linux
   (global-set-key (kbd "C-s-.") #'enlarge-window-horizontally)
   (global-set-key (kbd "C-s-,") #'shrink-window-horizontally)
   (global-set-key (kbd "C-s-+") #'enlarge-window)
   (global-set-key (kbd "C-s--") #'shrink-window))
  ('windows-nt
   (global-set-key (kbd "C-s-.") #'enlarge-window-horizontally)
   (global-set-key (kbd "C-s-,") #'shrink-window-horizontally)
   (global-set-key (kbd "C-s-+") #'enlarge-window)
   (global-set-key (kbd "C-s--") #'shrink-window)))

(pcase system-type
  ('darwin
   (global-set-key (kbd "M-‚") #'ace-swap-window) ; (alt-cmd-s)
   (global-set-key (kbd "M-∑") #'delete-frame) ; (alt-cmd-w)
   (global-set-key (kbd "M-≠") #'balance-windows) ; (alt-cmd-0)
   (global-set-key (kbd "M-ö") #'timu-func-split-and-follow-below)
   (global-set-key (kbd "M-Ö") #'timu-func-split-below)
   (global-set-key (kbd "M-ä") #'timu-func-split-and-follow-right)
   (global-set-key (kbd "M-Ä") #'timu-func-split-right)
   (global-set-key (kbd "M-œ") #'timu-func-split-bellow-all) ; (alt-cmd-ö)
   (global-set-key (kbd "M-æ") #'timu-func-toggle-split-direction) ; (alt-cmd-ä)
   (global-set-key (kbd "M-µ") #'bookmark-set) ; (alt-cmd-m)
   (global-set-key (kbd "µ") #'demap-toggle)) ; (alt-m)
  ('gnu/linux
   (global-set-key (kbd "C-s-s") #'ace-swap-window) ;; (alt-cmd-s)
   (global-set-key (kbd "C-s-w") #'delete-frame) ; (alt-cmd-w)
   (global-set-key (kbd "C-s-0") #'balance-windows) ; (alt-cmd-0)
   (global-set-key (kbd "C-ö") #'timu-func-split-and-follow-below)
   (global-set-key (kbd "C-Ö") #'timu-func-split-below)
   (global-set-key (kbd "C-ä") #'timu-func-split-and-follow-right)
   (global-set-key (kbd "C-Ä") #'timu-func-split-right)
   (global-set-key (kbd "C-s-ö") #'timu-func-split-bellow-all)
   (global-set-key (kbd "C-s-ä") #'timu-func-toggle-split-direction)
   (global-set-key (kbd "M-m") #'bookmark-set)
   (global-set-key (kbd "s-m") #'demap-toggle))
  ('windows-nt
   (global-set-key (kbd "C-s-s") #'ace-swap-window) ;; (alt-cmd-s)
   (global-set-key (kbd "C-s-w") #'delete-frame) ; (alt-cmd-w)
   (global-set-key (kbd "C-s-0") #'balance-windows) ; (alt-cmd-0)
   (global-set-key (kbd "C-ö") #'timu-func-split-and-follow-below)
   (global-set-key (kbd "C-Ö") #'timu-func-split-below)
   (global-set-key (kbd "C-ä") #'timu-func-split-and-follow-right)
   (global-set-key (kbd "C-Ä") #'timu-func-split-right)
   (global-set-key (kbd "C-s-ö") #'timu-func-split-bellow-all)
   (global-set-key (kbd "C-s-ä") #'timu-func-toggle-split-direction)
   (global-set-key (kbd "C-s-m") #'bookmark-set)
   (global-set-key (kbd "s-m") #'demap-toggle)))

;;;; buffers & files
(global-set-key (kbd "M-n") #'evil-buffer-new)
(global-set-key (kbd "M-w") #'timu-func-kill-current-buffer)
(global-set-key (kbd "M-q") #'save-buffers-kill-terminal)
(global-set-key (kbd "M-O") #'timu-func-find-file-as-root)
(global-set-key (kbd "M-o") #'find-file)
(global-set-key (kbd "M-i") #'insert-file)
(global-set-key (kbd "M-s") #'save-buffer)
(global-set-key (kbd "M-S") #'write-file) ; (save as; shift-cmd-s)
(global-set-key (kbd "C-M-s") #'save-some-buffers)

(pcase system-type
  ('darwin (global-set-key (kbd "C-∑") #'writeroom-mode)) ; (ctrl-alt-w)
  ('gnu/linux (global-set-key (kbd "M-s-w") #'writeroom-mode)) ; (ctrl-alt-w)
  ('windows-nt (global-set-key (kbd "C-s-w") #'writeroom-mode)))

;;;; consult
(global-set-key (kbd "M-f") #'consult-line)
(global-set-key (kbd "M-b") #'consult-buffer)
(global-set-key (kbd "M-V") #'consult-yank-pop)

(pcase system-type
  ('darwin (global-set-key (kbd "M-∫") #'consult-bookmark)) ; (alt-cmd-b)
  ((or 'gnu/linux 'windows-nt) (global-set-key (kbd "M-s-b") #'consult-bookmark)))

;;;; multiple-cursors
(global-set-key (kbd "C-M-<down>") 'mc/mark-next-like-this)
(global-set-key (kbd "C-M-<up>") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-M-<right>") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-M-<left>") 'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-M-.") 'mc/mark-all-like-this-dwim)
(global-set-key (kbd "C-M--") 'mc-hide-unmatched-lines-mode)

;;;; org-mode
(global-set-key (kbd "M-R") #'org-revert-all-org-buffers)

(pcase system-type
  ('darwin
   (global-set-key (kbd "å") #'org-archive-subtree) ; (alt-a)
   (global-set-key (kbd "M-ø") #'org-open-at-point-global))
  ((or 'gnu/linux 'windows-nt)
   (global-set-key (kbd "s-a") #'org-archive-subtree)
   (global-set-key (kbd "M-s-o") #'org-open-at-point-global)))

;;;; helpful
(global-set-key (kbd "C-h f") #'helpful-callable)
(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)

;;;; project
(global-set-key (kbd "M-p") #'timu-func-project-switch-project)

(pcase system-type
  ('darwin (global-set-key (kbd "M-π") #'project-find-file))
  ('windows-nt (global-set-key (kbd "M-s-p") #'project-find-file))
  ('gnu/linux (global-set-key (kbd "C-s-p") #'project-find-file)))

;;;; popper
(global-set-key (kbd "C-M-p") #'popper-toggle-latest)
(global-set-key (kbd "M-P") #'popper-toggle-type)

(pcase system-type
  ('darwin (global-set-key (kbd "C-π") #'popper-cycle))
  ('windows-nt (global-set-key (kbd "C-s-p") #'popper-cycle))
  ('gnu/linux (global-set-key (kbd "M-s-p") #'popper-cycle)))

;;;; custom functions
(pcase system-type
  ('darwin (global-set-key (kbd "M-ƒ") #'timu-func-consult-rg))
  ((or 'gnu/linux 'windows-nt) (global-set-key (kbd "C-s-f") #'timu-func-consult-rg)))
(global-set-key (kbd "C-M-e") #'timu-func-embark-act)

;;;; emacs-plus or emacsmacport:
(when
    (custom-variable-p 'mac-effective-appearance-change-hook)
  (global-set-key (kbd "s-u") #'revert-buffer))


;;; TIMU-KEYMAP
;;;; timu-map prefix
;; free the binding from evil first
(evil-global-set-key 'normal (kbd "C-t") nil)
(evil-global-set-key 'insert (kbd "C-t") nil)
(evil-global-set-key 'visual (kbd "C-t") nil)
(evil-global-set-key 'operator (kbd "C-t") nil)
(evil-global-set-key 'replace (kbd "C-t") nil)
(evil-global-set-key 'motion (kbd "C-t") nil)

;; define the prefix and map
(define-prefix-command 'timu-map)
(global-set-key (kbd "C-t") #'timu-map)

;;;; timu-keys-general-map
(defvar timu-keys-general-map
  (let ((map (make-sparse-keymap)))
    (define-key map "e" #'embark-act)
    (define-key map "d" #'dired)
    (define-key map "g" #'magit-status)
    (define-key map "t" #'tab-bar-switch-to-next-tab)
    (define-key map ","#'timu-func-visit-emacs-init)
    (define-key map ";"#'timu-func-visit-emacs-early-init)
    map))
(fset 'timu-keys-general-map timu-keys-general-map)
(define-key timu-map "g" 'timu-keys-general-map)

;;;; timu-keys-buffer-map
(defvar timu-keys-buffer-map
  (let ((map (make-sparse-keymap)))
    (define-key map "b" #'consult-buffer)
    (define-key map "e" #'eval-buffer)
    (define-key map "i" #'ibuffer)
    (define-key map "w" #'kill-current-buffer)
    (define-key map "r" #'revert-buffer)
    map))
(fset 'timu-keys-buffer-map timu-keys-buffer-map)
(define-key timu-map "b" 'timu-keys-buffer-map)

;;;; timu-keys-file-map
(defvar timu-keys-file-map
  (let ((map (make-sparse-keymap)))
    (define-key map "F" #'timu-func-find-file-as-root)
    (define-key map "f" #'find-file)
    (define-key map "m" #'timu-func-move-buffer-file)
    (define-key map "o" #'find-file-other-window)
    (define-key map "r" #'timu-func-rename-file-and-buffer)
    (define-key map "s" #'save-buffer)
    (define-key map "w" #'write-file)
    map))
(fset 'timu-keys-file-map timu-keys-file-map)
(define-key timu-map "f" 'timu-keys-file-map)

;;;; timu-keys-frames-windows-map
(defvar timu-keys-frames-windows-map
  (let ((map (make-sparse-keymap)))
    (define-key map "Ä" #'timu-func-toggle-split-direction)
    (define-key map "ä" #'timu-func-split-and-follow-right)
    (define-key map "ö" #'timu-func-split-and-follow-below)
    (define-key map "0" #'delete-window)
    (define-key map "1" #'delete-other-windows)
    (define-key map "2" #'ace-delete-window)
    (define-key map "N" #'make-frame-command)
    (define-key map "l" #'windmove-left)
    (define-key map "r" #'windmove-right)
    (define-key map "u" #'windmove-up)
    (define-key map "d" #'windmove-down)
    map))
(fset 'timu-keys-frames-windows-map timu-keys-frames-windows-map)
(define-key timu-map "w" 'timu-keys-frames-windows-map)

;;;; timu-keys-project-map
(defvar timu-keys-project-map
  (let ((map (make-sparse-keymap)))
    (define-key map "p" 'timu-func-project-switch-project)
    (define-key map "P" 'project-find-file)
    map))
(fset 'timu-keys-project-map timu-keys-project-map)
(define-key timu-map "p" 'timu-keys-project-map)

;;;; timu-keys-help-map
(defvar timu-keys-help-map
  (let ((map (make-sparse-keymap)))
    (define-key map "b" 'describe-bindings)
    (define-key map "F" 'describe-face)
    (define-key map "f" 'helpful-callable)
    (define-key map "i" 'info)
    (define-key map "k" 'helpful-key)
    (define-key map "m" 'describe-bindings)
    (define-key map "p" 'describe-package)
    (define-key map "v" 'helpful-variable)
    map))
(fset 'timu-keys-help-map timu-keys-help-map)
(define-key timu-map "h" 'timu-keys-help-map)


;;; GLOBAL EVIL KEYBINDINGS
;;;; windows
(evil-global-set-key 'normal (kbd "S") #'ace-link)
(evil-global-set-key 'normal (kbd "ö") #'ace-window)

;;;; files
(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "œ") #'timu-func-find-file-below)
   (evil-global-set-key 'normal (kbd "æ") #'timu-func-find-file-right))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-ö") #'timu-func-find-file-below)
   (evil-global-set-key 'normal (kbd "s-ä") #'timu-func-find-file-right)))

;;;; tabs
(evil-global-set-key 'normal (kbd "M-t") #'tab-bar-switch-to-next-tab)

(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "†") #'timu-func-tab-bar-new-tab) ; alt-t
   (evil-global-set-key 'normal (kbd "˝") #'tab-bar-close-tab)) ; shift-alt-t
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-t") #'timu-func-tab-bar-new-tab)
   (evil-global-set-key 'normal (kbd "s-T") #'tab-bar-close-tab)))

;;;; avy
(evil-global-set-key 'normal (kbd "s") #'avy-goto-char-timer)

;;;; org-mode
(evil-global-set-key 'normal (kbd "M-l") #'org-insert-link)

;;;; imenu & neotree
(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "⁄") #'imenu-list-smart-toggle) ; alt-i
   (evil-global-set-key 'normal (kbd "¿") #'neotree-toggle)) ; alt-ß
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-i") #'imenu-list-smart-toggle)
   (evil-global-set-key 'normal (kbd "s-ß") #'neotree-toggle)))

;;;; pyvenv
(pcase system-type
  ('darwin
   (evil-global-set-key 'normal (kbd "∏") #'pyvenv-activate)
   (evil-global-set-key 'normal (kbd "π") #'pyvenv-deactivate))
  ((or 'gnu/linux 'windows-nt)
   (evil-global-set-key 'normal (kbd "s-P") #'pyvenv-activate)
   (evil-global-set-key 'normal (kbd "s-p") #'pyvenv-deactivate)))


;;; EVIL-LEADER
(require 'timu-evil-leader)


(provide 'timu-keys)

;;; timu-keys.el ends here
