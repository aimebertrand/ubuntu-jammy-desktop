;;; timu-transients.el --- Custom Transient commands -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1") (transient "0.3.7"))
;; Created: 2020-10-03
;; Keywords: tools convenience transient keybinding
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my created transients for easy finding of functions.
;; The Transients are broken up for each major mode.

;;; Code:


(require 'smerge-mode)
(require 'transient)


;;; DEFAULTS
(defgroup timu-transients ()
  "Customise group for the `timu-transients' Library."
  :group 'timu)

(defcustom timu-transients-path
  (expand-file-name "libraries/timu-transients.el" user-emacs-directory)
  "Variable for the path of the module `timu-transients'."
  :type 'file
  :group 'timu-transients)

(customize-set-variable
 'transient-history-file
 (expand-file-name "local/history.el" user-emacs-directory))


;;; TRANSIENTS TRANSIENT
(transient-define-prefix timu-transients-transient ()
  "Transient for to select one of my custom transient commands.
Will list the following transients to select from:
`timu-transients-dired', `timu-transients-smerge',
`timu-transients-pdf-view', `timu-transients-org-remark'."
  ["LIST OF TRANSIENT COMMANDS\n"
   ["TRANSIENTS"
    ("d" "Dired" timu-transients-dired)
    ("o" "Org Remark" timu-transients-org-remark)
    ("p" "PDF-View" timu-transients-pdf-view)
    ("s" "Smerge" timu-transients-smerge)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])


;;; DIRED TRANSIENTS
(transient-define-prefix timu-transients-dired ()
  "Transient for DIRED BUFFERS."
  ["DIRED TRANSIENT COMMANDS\n"
   ["FILES"
    ("c" "Compress files(s) to" dired-do-compress-to) ; compress files into a separate files
    ("C" "Copy files(s)" dired-do-copy) ; compress files in place to gz
    ("D" "Delete files(s)" dired-do-delete)
    ("R" "Rename files(s)" dired-do-rename)
    ("S" "Symlink files(s)" dired-do-symlink)
    ("T" "Touch files(s)" dired-do-touch)
    ("Z" "Compress files(s)" dired-do-compress)]
   ["ACTIONS"
    ("+" "Create directory" dired-create-directory)
    ("a" "Attach to Mail" gnus-dired-attach)
    ("o" "Open with" timu-func-dired-shell-open)
    ("ö" "Open marked" timu-func-dired-find-marked-file)]
   ["PERMISSIONS"
    ("G" "Chmod" dired-do-chgrp)
    ("M" "Chmod" dired-do-chmod)
    ("O" "Chown" dired-do-chown)]
   ["MARKS"
    ("/" "Mark directories" dired-mark-directories)
    ("%" "Mark regexp" dired-mark-files-regexp)
    ("@" "Mark symlinks" dired-mark-symlinks)
    ("m" "Set mark" dired-mark)
    ("u" "Unset mark" dired-unmark)
    ("U" "Unset all mark" dired-unmark-all-marks)
    ("t" "Toggle mark" dired-toggle-marks)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])


;;; TRANSIENT FOR SMERGE FUNCTIONS
(transient-define-prefix timu-transients-smerge ()
  "Transient for SMERGE MODE."
  ["SMERGE MANUE\n"
   ["ACTIONS"
    ("r" "Smerge resolve" smerge-resolve)
    ("a" "Smerge keep-all" smerge-keep-all)
    ("b" "Smerge keep-base" smerge-keep-base)
    ("l" "Smerge keep-lower" smerge-keep-lower)
    ("u" "Smerge keep-upper" smerge-keep-upper)
    ("E" "Smerge ediff" smerge-ediff)]
   ["NAVIGATION"
    ("n" "Smerge next" smerge-next)
    ("p" "Smerge prev" smerge-prev)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])


;;; ORG-REMARK
(transient-define-prefix timu-transients-org-remark ()
  "Transient function to call `org-remark' commands/functions."
  ["ORG-REMARK TRANSIENT\n"
   ["MARKS"
    ("c" "Insert a cyan org remark" org-remark-mark-timu-darkcyan)
    ("m" "Insert a magenta org remark" org-remark-mark-timu-magenta)
    ("o" "Insert an orange org remark" org-remark-mark-timu-orange)
    ("y" "Insert a yellow org remark" org-remark-mark-timu-yellow)]
   ["OTHER COMMANDS"
    ("C" "Change the org remark at point" org-remark-change)
    ("n" "Go to next org remark" org-remark-next)
    ("p" "Go to previous org remark" org-remark-prev)
    ("O" "Open org remark margin buffer" org-remark-open)
    ("r" "Remove org remarks" org-remark-remove)
    ("t" "Toggle org remarks" org-remark-toggle)]
   ["TRANSIENT CMD\n"
    ("<escape>" "Quit Transient" keyboard-quit)]])


;;; TRANSIENT KEYBINDINGS
(if timu-defaults-wsl-p
    (progn
      (define-key dired-mode-map (kbd "C-#") 'timu-transients-dired)
      (define-key smerge-mode-map (kbd "C-#") 'timu-transients-smerge)
      (define-key org-remark-mode-map (kbd "C-#") 'timu-transients-org-remark))
  (pcase system-type
    ((or 'darwin 'gnu/linux)
     (define-key dired-mode-map (kbd "M-#") 'timu-transients-dired)
     (define-key smerge-mode-map (kbd "M-#") 'timu-transients-smerge)
     (define-key org-remark-mode-map (kbd "M-#") 'timu-transients-org-remark))
    ('windows-nt
     (define-key dired-mode-map (kbd "M-$") 'timu-transients-dired)
     (define-key smerge-mode-map (kbd "M-$") 'timu-transients-smerge)
     (define-key org-remark-mode-map (kbd "M-$") 'timu-transients-org-remark))))

(define-key transient-map (kbd "<escape>") #'transient-quit-one)


(provide 'timu-transients)

;;; timu-transients.el ends here
