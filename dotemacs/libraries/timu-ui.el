;;; timu-ui.el --- Config for the look and feel -*- lexical-binding: t -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: tools ui ux look feel
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the customization of the Emacs interface.

;;; Code:


(require 'doom-modeline)
(require 'mixed-pitch)
(require 'timu-spacegrey-theme)


;;; DEFAULTS
(defgroup timu-ui ()
  "Customise group for the `timu-ui' Library."
  :group 'timu)

(defcustom timu-ui-path
  (expand-file-name "libraries/timu-ui.el" user-emacs-directory)
  "Variable for the path of the module `timu-ui'."
  :type 'file
  :group 'timu-ui)

(defcustom timu-ui-custom-dark-theme 'timu-spacegrey
  "Variable dark theme to be used in to store the theme I want to use.
Used in `timu-func-theme-hook-28' & `timu-func-theme-hook-28' as well."
  :type 'symbol
  :group 'timu-ui)

(defcustom timu-ui-custom-light-theme 'timu-spacegrey
  "Variable light theme to be used in to store the theme I want to use.
Used in `timu-func-theme-hook-28' & `timu-func-theme-hook-28' as well."
  :type 'symbol
  :group 'timu-ui)


;;; LOOK AND FEEL
;;;; fullscreen for linux
(pcase system-type ('gnu/linux (add-hook 'after-init-hook #'toggle-frame-maximized)))

(pcase timu-defaults-gdm-session
  ("sway" (add-hook 'after-init-hook
                    (lambda ()
                      (setq-default header-line-format "")))))

;;;; theme settings
;;;;; custom themes
(add-to-list 'custom-theme-load-path (expand-file-name "themes" user-emacs-directory))
(add-to-list 'custom-theme-load-path "~/projects/pdotemacs/themes")
(load-theme timu-ui-custom-dark-theme t)


;;; SET FACES
(pcase system-type
  ('darwin (setq timu-default-face-height 140))
  ('gnu/linux
   (if timu-defaults-wsl-p
       (setq timu-default-face-height 110)
     (setq timu-default-face-height 130)))
  ('windows-nt (setq timu-default-face-height 110)))

(set-face-attribute 'default nil
                    :family "JetBrains Mono"
                    :weight 'light
                    :height timu-default-face-height)

(set-face-attribute 'fixed-pitch nil
                    :family "JetBrains Mono"
                    :weight 'light
                    :height timu-default-face-height)

(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Aile"
                    :weight 'medium
                    :height timu-default-face-height)


;;; MODELINE
;;;; modeline settings
(minions-mode 1)
(customize-set-variable 'line-number-mode t)
(customize-set-variable 'column-number-mode t)

;;;; doom-modeline
(doom-modeline-mode 1)

(customize-set-variable 'doom-modeline-buffer-encoding nil)
(customize-set-variable 'doom-modeline-enable-word-count t)
(customize-set-variable 'doom-modeline-icon t)
(customize-set-variable 'doom-modeline-major-mode-icon t)
(customize-set-variable 'doom-modeline-minor-modes (featurep 'minions))
(customize-set-variable 'doom-modeline-modal-icon nil)

;;;; modeline evil-states tags
(setq evil-emacs-state-tag (propertize "-E-")
      evil-normal-state-tag (propertize "-N-")
      evil-insert-state-tag (propertize "-I-")
      evil-motion-state-tag (propertize "-M-")
      evil-visual-state-tag (propertize "-V-")
      evil-operator-state-tag (propertize "-O-")
      evil-replace-state-tag (propertize "-R-"))


;;; WINDOWS BEHAVIOR
;;;; display buffer
(customize-set-variable
 'display-buffer-alist
 '(("\\*Ilist\\*"
    (display-buffer-in-side-window)
    (side . right)
    (window-width . 0.20))

   ("\\*Messages\\*"
    (display-buffer-in-side-window)
    (side . bottom)
    (window-height . 0.30))

   ("\\(\\*tex-shell\\*\\|*Async Shell Command*\\)"
    (display-buffer-in-side-window)
    (side . bottom)
    (window-height . 0.30))

   ("\\(\\*Help\\*\\|\\*helpful .*\\|\\*info*\\)"
    (display-buffer-in-side-window)
    (side . right)
    (window-width . 0.4))

   ("\\(\\*pylsp\\*\\|\\*pylsp.*stderr\\*\\|\\*lsp-log\\*\\)"
    (display-buffer-at-bottom)
    (window-height . 0.30))

   ("^magit: *"
    (display-buffer-below-selected)
    (window-height . 0.7))

   ("\\(^magit-diff: *\\|^magit-process: *\\|^magit-revision: *\\)"
    (display-buffer-below-selected)
    (window-height . 0.4))

   (".*new-comment.*"
    (display-buffer-below-selected)
    (window-height . 0.4))))


;;; POPPER
(require 'popper)
(customize-set-variable 'popper-reference-buffers
                        '("\\*Async Shell Command\\*"
                          "\\*Ilist\\*"
                          "\\*info\\*"
                          "^magit: *"
                          "^magit-diff: *"
                          "^magit-process: *"
                          "\\*Messages\\*"
                          "\\*lsp-log\\*"
                          "Output\\*$"
                          "\\*osx-dictionary\\*"
                          "\\*pylsp.*stderr\\*"
                          "\\*pylsp\\*"
                          compilation-mode
                          help-mode
                          helpful-mode))

(customize-set-variable 'popper-mode-line
                        '(:eval (propertize " POP " 'face 'mode-line-emphasis)))

(customize-set-variable 'popper-display-control nil)

(setq popper-group-function #'popper-group-by-project) ; project.el projects
;; (setq popper-group-function #'popper-group-by-directory) ; fallback

(popper-mode +1)


;;; EDITOR IN WINDOW SETTINGS
;;;; custom settings for the editor part
(customize-set-variable 'line-spacing 0.3)
(global-hl-line-mode 1)
(customize-set-variable 'scroll-conservatively 100)
(customize-set-variable 'scroll-preserve-screen-position 1)

;;;; no tabs
(customize-set-variable 'indent-tabs-mode nil)
(customize-set-variable 'tab-width 4)

;;;; UTF-8 encoding
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;;;; preserve contents of system clipboard
;; Puts System clipboard stuff in the kill ring
(customize-set-variable 'save-interprogram-paste-before-kill t)

;;;; visual-line-mode
(add-hook 'prog-mode-hook #'visual-line-mode)
(add-hook 'text-mode-hook #'visual-line-mode)

;;;; line numbers
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)

;;;; mixed pitch
(add-hook 'org-mode-hook #'mixed-pitch-mode)

(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-from)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-name)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-content)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-subject)
(add-to-list 'mixed-pitch-fixed-pitch-faces 'gnus-header-newsgroups)


;;; ALL-THE-ICONS-COMPLETION
(all-the-icons-completion-mode)
(add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)


;;; RAINBOW-DELIMITERS
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)


;;; OTHER MINOR CHANGES
;;;; alarm bell
(customize-set-variable 'visible-bell nil)
(customize-set-variable 'ring-bell-function 'timu-func-flash-mode-line)

;;;; delete trailing whitespace
(add-hook 'before-save-hook #'delete-trailing-whitespace)


(provide 'timu-ui)

;;; timu-ui.el ends here
