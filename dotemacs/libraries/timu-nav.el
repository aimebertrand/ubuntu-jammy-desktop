;;; timu-nav.el --- Config for navigation -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper navigation
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for all thing navigation.

;;; Code:


(require 'neotree)
(require 'prog-mode)


;;; DEFAULTS
(defgroup timu-nav ()
  "Customise group for the `timu-nav' Library."
  :group 'timu)

(defcustom timu-nav-path
  (expand-file-name "libraries/timu-nav.el" user-emacs-directory)
  "Variable for the path of the module `timu-nav'."
  :type 'file
  :group 'timu-nav)


;;; BUFFERS
;; kill buffers without confirmation
(setq kill-buffer-query-functions
      (delq 'process-kill-buffer-query-function kill-buffer-query-functions))


;;; IBUFFER
;;;; expert-mode
;; killing buffers or groups without confirmation
(customize-set-variable 'ibuffer-expert t)

;;;; groups
(customize-set-variable
 'ibuffer-saved-filter-groups
 (quote (("default"
          ("custom" (mode . Custom-mode))
          ("dired" (mode . dired-mode))
          ("docker" (name . "^*docker-*"))
          ("elisp" (mode . emacs-lisp-mode))
          ("org" (mode . org-mode))
          ("git" (or (name . "^magit*")
                     (name . "^*Ediff*")
                     (name . "^*ediff-*")
                     (name . "^\\*forge: *")))
          ("help" (or (mode . helpful-mode)
                      (mode . help-mode)))
          ("programming" (or (mode . python-mode)
                             (mode . go-mode)
                             (mode . sh-mode)))
          ("shell" (or (mode . eshell-mode)
                       (mode . shell-mode)
                       (name . "*ansi-term*")
                       (name . "*shell*")))
          ("Slack" (name . "^*Slack*"))
          ("tramp" (name . "^*tramp*"))
          ("web" (or (mode . web-mode)
                     (mode . js2-mode)))
          ("emacs" (or (name . "*scratch*")
                       (name . "*Messages*")))
          ("errors/logs" (or (name . "*Async-native-c*")
                             (name . "*Backtrace*")
                             (name . "*Compile-Log*")
                             (name . "*Warnings*")))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (all-the-icons-ibuffer-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))

;; Don't show filter groups if there are no buffers in that group
(customize-set-variable 'ibuffer-show-empty-filter-groups nil)

;;;; ibuffer keybindings
(with-eval-after-load 'ibuffer
  (define-key ibuffer-mode-map [remap ibuffer-backward-filter-group]
    #'timu-func-project-switch-project))


;;; DEMAP
(customize-set-variable 'demap-minimap-window-side 'right)
(customize-set-variable 'demap-minimap-window-width 40)


;;; IMENU-LIST
(add-hook 'imenu-list-minor-mode-hook (lambda () (doom-modeline-mode +1)))
(customize-set-variable 'imenu-list-position 'right)


;;; TAB NAVIGATION
(customize-set-variable 'tab-bar-show nil)


;;; ACE-WINDOW
(customize-set-variable 'aw-keys '(?u ?i ?o ?p ?h ?j ?k ?l))


;;; ACE-LINK
(evil-define-key 'normal Info-mode-map (kbd "S") #'ace-link)


;;; NEOTREE
;;;; neotree settings
(customize-set-variable 'neo-theme (if (display-graphic-p) 'icons 'arrow))

;;;; neotree keybindings
(define-key neotree-mode-map (kbd "M-RET") #'neotree-change-root)
(define-key neotree-mode-map (kbd "M-<up>") #'neotree-select-up-node)
(define-key neotree-mode-map (kbd "M-…") #'neotree-stretch-toggle)
(define-key neotree-mode-map (kbd "M-ø") #'neotree-dir)
(define-key neotree-mode-map (kbd "<S-return>")
  (neotree-make-executor :file-fn 'neo-open-file-ace-window))
(evil-define-key 'normal neotree-mode-map (kbd "æ")
  (neotree-make-executor :file-fn 'neo-open-file-vertical-split))
(evil-define-key 'normal neotree-mode-map (kbd "œ")
  (neotree-make-executor :file-fn 'neo-open-file-horizontal-split))


;;; WINNER-MODE
(add-hook 'after-init-hook 'winner-mode)


(provide 'timu-nav)

;;; timu-nav.el ends here
