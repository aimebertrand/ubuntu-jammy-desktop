;;; timu-defaults.el --- Defaults for my configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-03-27
;; Keywords: defaults tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my defaults to be loaded in the beginning.

;;; Code:


(require 'server)


;;; DEFAULTS
(defgroup timu-defaults ()
  "Customise group for the `timu-defaults' Library."
  :group 'timu)

(defcustom timu-defaults-path
  (expand-file-name "libraries/timu-defaults.el" user-emacs-directory)
  "Variable for the path of the module `timu-defaults'."
  :type 'file
  :group 'timu-defaults)

(defcustom timu-defaults-gdm-session (shell-command-to-string "printf $GDMSESSION")
  "Variable to store the Desktop environement on linux."
  :type 'string
  :group 'timu-defaults)

(defcustom timu-defaults-wsl-string nil
  "Variable to store the string of the Hypervisor.
This useful in case we are on a wsl GNU/Linux distro."
  :type 'string
  :group 'timu-defaults)

(defcustom timu-defaults-wsl-p nil
  "Variable to store the info wheter we are in a wsl GNU/Linux distro."
  :type 'boolean
  :group 'timu-defaults)


;;; SERVER MODE
(unless (server-running-p)
  (server-start))


;;; GLOBAL DEFAULTS
;;;; environment
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (exec-path-from-shell-initialize)
   (exec-path-from-shell-copy-envs '("PATH" "LC_ALL" "SHELL" "SSH_AUTH_SOCK"))))

;;;; variables
(customize-set-variable 'timu-defaults-wsl-string
                        (shell-command-to-string
                         "printf $(lscpu | grep Hypervisor | awk '{print $3}')"))

(if (equal "Microsoft" timu-defaults-wsl-string)
    (customize-set-variable 'timu-defaults-wsl-p t)
  (customize-set-variable 'timu-defaults-wsl-p nil))

(setq default-directory "~/")

(if (boundp 'use-short-answers)
    (customize-set-variable 'use-short-answers t)
  (advice-add 'yes-or-no-p :override #'y-or-n-p))

(customize-set-variable
 'bookmark-file
 (expand-file-name "local/bookmarks" user-emacs-directory))
(customize-set-variable
 'project-list-file
 (expand-file-name "local/projects" user-emacs-directory))
(customize-set-variable
 'tramp-persistency-file-name
 (expand-file-name "local/tramp" user-emacs-directory))
(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory))
(customize-set-variable 'global-auto-revert-non-file-buffers t)
(customize-set-variable 'load-prefer-newer t)
(customize-set-variable 'confirm-kill-processes nil)
(customize-set-variable 'kill-do-not-save-duplicates t)
(customize-set-variable 'tab-always-indent 'complete)
(customize-set-variable 'display-fill-column-indicator-column 79)
(customize-set-variable 'warning-minimum-level :error)
(customize-set-variable 'help-window-select t)
(customize-set-variable 'display-buffer-base-action
                        '((display-buffer-in-previous-window
                           display-buffer-reuse-window
                           display-buffer-use-some-window
                           display-buffer--maybe-pop-up-frame-or-window
                           display-buffer-in-side-window
                           display-buffer-pop-up-frame)))
(put 'narrow-to-region 'disabled nil)

;;;; recent files
(customize-set-variable 'recentf-save-file
                        (expand-file-name "local/recentf" user-emacs-directory))
(customize-set-variable 'recentf-max-saved-items 20)
(customize-set-variable 'recentf-exclude
                        (list "COMMIT_EDITMSG"
                              "~$"
                              "/tmp/"
                              "/ssh:"
                              "/sudo:"
                              "/scp:"
                              ;; binary
                              "\\.mkv$"
                              "\\.mp[34]$"
                              "\\.avi$"
                              "\\.pdf$"
                              "\\.docx?$"
                              "\\.xlsx?$"))

;;;; backups and auto-saves
;; store all backup and autosave files in the tmp dir
(customize-set-variable 'backup-directory-alist
                        `((".*" . ,temporary-file-directory)))

(customize-set-variable 'auto-save-file-name-transforms
                        `((".*" ,temporary-file-directory t)))

(customize-set-variable
 'auto-save-list-file-prefix
 (expand-file-name "emacs-auto-save-list/.saves-" temporary-file-directory))

;;;; undo-tree
(global-undo-tree-mode)
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (customize-set-variable 'undo-tree-history-directory-alist
                           `(("." . ,temporary-file-directory))))
  ('windows-nt
   (customize-set-variable
    'undo-tree-history-directory-alist
    '(("." . "~/.emacs.d/undo-tree-history")))))

;;;; auth-sources paths
(add-to-list 'auth-sources "~/projects/pdotemacs/emacs-authinfo")


;;; MY INFO
(customize-set-variable 'user-full-name "Aimé Bertrand")
(customize-set-variable 'user-mail-address "aime.bertrand@macowners.club")


;;; TRASH
(customize-set-variable 'delete-by-moving-to-trash t)


(provide 'timu-defaults)
;;; timu-defaults.el ends here
