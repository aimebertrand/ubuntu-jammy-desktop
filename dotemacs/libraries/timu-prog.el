;;; timu-prog.el --- Programming languages config -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-01
;; Keywords: languages tools helper python go swift
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides my config for coding, languages & Co.

;;; Code:


(require 'lsp-sourcekit)


;;; DEFAULTS
(defgroup timu-prog ()
  "Customise group for the `timu-prog' Library."
  :group 'timu)

(defcustom timu-prog-path
  (expand-file-name "libraries/timu-prog.el" user-emacs-directory)
  "Variable for the path of the module `timu-prog'."
  :type 'file
  :group 'timu-prog)


;;; FLYMAKE
;;;; flymake for sh-mode
(customize-set-variable 'flymake-shellcheck-use-file nil)

(add-hook 'sh-mode-hook #'flymake-mode)
(add-hook 'sh-mode-hook #'flymake-shellcheck-load)

;;;; flymake hint at point
(add-hook 'flymake-mode-hook #'flymake-diagnostic-at-point-mode)


;;; COMPANY
(customize-set-variable 'company-minimum-prefix-length 1)
(customize-set-variable 'company-idle-delay 0.0)

(define-key company-active-map (kbd "<tab>") #'timu-func-company-complete-selection)
(define-key lsp-mode-map (kbd "<tab>") #'company-indent-or-complete-common)

(add-hook 'emacs-lisp-mode-hook #'company-mode)
(add-hook 'company-mode-hook #'company-box-mode)
(add-hook 'lsp-mode-hook #'company-mode)
(add-hook 'sh-mode-hook #'company-mode)

;;;; company-backends
(add-to-list 'company-backends 'company-shell)
(add-to-list 'company-backends 'company-elisp)
(add-to-list 'company-backends 'company-anaconda)


;;; LSP
;;;; lsp config
(add-hook 'lsp-mode-hook #'lsp-ui-mode)

(customize-set-variable 'lsp-ui-doc-position 'top)
(customize-set-variable
 'lsp-session-file
 (expand-file-name "local/.lsp-session-v1" user-emacs-directory))
(customize-set-variable 'lsp-ui-doc-use-webkit nil)
(customize-set-variable 'lsp-ui-doc-delay 0)

;;;; lsp keybindings
(evil-define-key 'normal lsp-mode-map (kbd "g d") #'lsp-find-definition)
(evil-define-key 'normal lsp-mode-map (kbd "g i") #'lsp-find-implementations)
(evil-define-key 'normal lsp-mode-map (kbd "g l") #'lsp-find-locations)
(evil-define-key 'normal lsp-mode-map (kbd "g r") #'lsp-find-references)


;;; EGLOT
(add-hook 'sh-mode-hook #'eglot-ensure)
(add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-at-point-mode)

;;;; eglot keybindings
(evil-define-key 'normal eglot-mode-map (kbd "g d") #'eglot-find-declaration)
(evil-define-key 'normal eglot-mode-map (kbd "g i") #'eglot-find-implementations)


;;; PYTHON
;;;; python settings
;; Use IPython for REPL
(customize-set-variable 'python-shell-interpreter "python3")
(customize-set-variable 'python-shell-interpreter-args "-i")
(customize-set-variable 'python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters "python3")

(add-hook 'python-mode-hook #'lsp-deferred)
(add-hook 'python-mode-hook #'pyvenv-mode)

(add-hook 'python-mode-hook #'auto-virtualenv-set-virtualenv)
(add-hook 'window-configuration-change-hook #'auto-virtualenv-set-virtualenv)
(add-hook 'focus-in-hook #'auto-virtualenv-set-virtualenv)

(add-to-list 'auto-mode-alist '("\\requirements.txt\\'" . pip-requirements-mode))

;;;; python keybindings
(define-key python-mode-map (kbd "M-<left>") #'evil-window-left)
(define-key python-mode-map (kbd "M-<right>") #'evil-window-right)
(define-key python-mode-map (kbd "M-<up>") #'evil-window-up)
(define-key python-mode-map (kbd "M-<down>") #'evil-window-down)

(define-key inferior-python-mode-map (kbd "M-<left>") #'evil-window-left)
(define-key inferior-python-mode-map (kbd "M-<right>") #'evil-window-right)
(define-key inferior-python-mode-map (kbd "M-<up>") #'evil-window-up)
(define-key inferior-python-mode-map (kbd "M-<down>") #'evil-window-down)


;;; GO
(add-hook 'go-mode-hook #'lsp-deferred)

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(add-hook 'go-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'lsp-format-buffer t t)
            (add-hook 'before-save-hook #'lsp-organize-imports t t)))


;;; SWIFT
;;;; lsp-sourcekit
(setq lsp-sourcekit-executable
      (string-trim (shell-command-to-string "xcrun --find sourcekit-lsp")))

(add-hook 'swift-mode-hook #'lsp-deferred)


(provide 'timu-prog)

;;; timu-prog.el ends here
