;;; timu-org.el --- Org mode configuration -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-02-14
;; Keywords: orgmode org tools helper
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file contains my org mode configuration.

;;; Code:


(require 'noflet)
(require 'org-remark)
(require 'org-bullets)


;;; DEFAULTS
(defgroup timu-org ()
  "Customise group for the `timu-org' Library."
  :group 'timu)

(defcustom timu-org-path
  (expand-file-name "libraries/timu-org.el" user-emacs-directory)
  "Variable for the path of the module `timu-org'."
  :type 'file
  :group 'timu-org)


;;; ORG-CONFIG
;; org-mode variables
(customize-set-variable 'org-image-actual-width nil)
(customize-set-variable 'org-cycle-hook '(org-cycle-hide-archived-subtrees
                                          org-cycle-show-empty-lines
                                          org-optimize-window-after-visibility-change))
(customize-set-variable 'org-agenda-files (list "~/org/files"))
(customize-set-variable
 'org-id-locations-file
 (expand-file-name "local/.org-id-locations" user-emacs-directory))
(customize-set-variable 'org-directory "~/org")
(customize-set-variable 'org-publish-timestamp-directory
                        (expand-file-name "org-timestamps" user-emacs-directory))
(customize-set-variable 'org-return-follows-link t)
(customize-set-variable 'org-use-sub-superscripts nil)
(customize-set-variable 'org-src-window-setup 'split-window-below)
(customize-set-variable 'org-link-frame-setup '((file . find-file)))
(customize-set-variable 'org-log-done 'time)
(customize-set-variable 'org-confirm-babel-evaluate nil)
(customize-set-variable 'org-hide-emphasis-markers t)
(customize-set-variable 'org-html-validation-link nil)
(customize-set-variable 'org-emphasis-alist
                        '(("*" timu-spacegrey-bold-face)
                          ("/" timu-spacegrey-italic-face)
                          ("_" timu-spacegrey-underline-face)
                          ("=" org-verbatim verbatim)
                          ("~" org-code verbatim)
                          ("+" timu-spacegrey-strike-through-face)))


;; org-babel languages
(pcase system-type
  ((or 'darwin 'gnu/linux)
   (org-babel-do-load-languages
    'org-babel-load-languages
    '((C . nil)
      (awk . t)
      (clojure . t)
      (css . t)
      (emacs-lisp . t)
      (http . t)
      (java . t)
      (js . t)
      (latex . t)
      (lisp . t)
      (python . t)
      (restclient . t)
      (shell . t)))))

;; org download
(customize-set-variable 'org-download-image-dir "~/Pictures")
;; drag-and-drop to `dired`
(add-hook 'dired-mode-hook #'org-download-enable)
;; annoying indentation after RET
(add-hook 'org-mode-hook (lambda () (electric-indent-mode -1)))

;; custom states
(customize-set-variable 'org-todo-keywords
                        '((sequence "TODO(@t/!)" "PROGRESS(p)" "WAITING(w)" "|" "DONE(@d/!)" "CANCELLED(@c/!)")
                          (sequence "MAIL(@t/!)" "|" "DONE(@d/!)")))

;; custom colors for states
(customize-set-variable 'org-todo-keyword-faces
                        '(("TODO" . "#BF616A")
                          ("MAIL" . "#BF616A")
                          ("PROGRESS" . "#46D9FF")
                          ("WAITING" . "#ECBE7B")
                          ("DONE" . "#A3BE8C")
                          ("CANCELED" . "#D08770")))

;; colors for priorities
(customize-set-variable 'org-priority-faces '((?A . (:foreground "#BF616A" :weight bold))
                                              (?B . (:foreground "#ECBE7B"))
                                              (?C . (:foreground "#A3BE8C"))))


;; org refile & archive save advices
(defadvice org-revert-all-org-buffers
    (around auto-confirm compile activate)
  "Advice to revert all org buffers after refiling."
  (cl-letf (((symbol-function 'yes-or-no-p) (lambda (&rest args) t))
            ((symbol-function 'y-or-n-p) (lambda (&rest args) t))) ad-do-it))

(advice-add 'org-refile :after #'org-save-all-org-buffers)
(advice-add 'org-archive-subtree :after #'org-save-all-org-buffers)
(advice-add 'org-save-all-org-buffers :after #'org-revert-all-org-buffers)


;;; ORG-CAPTURE
;;;; org-protocol
(add-to-list 'org-modules 'org-protocol)
(customize-set-variable 'org-blank-before-new-entry '((heading . nil)
                                                      (plain-list-item . nil)))

;; bind key to: emacsclient -ne "(timu-func-make-capture-frame)"
;; with a macos Automator.app created Service
(defadvice org-capture-finalize
    (after delete-capture-frame activate)
  "Advise capture-finalize to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

(defadvice org-capture-destroy
    (after delete-capture-frame activate)
  "Advise capture-destroy to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

;;;; org-capture templates
(add-to-list 'org-capture-templates
             '("p" "private templates"))

(add-to-list 'org-capture-templates
             '("pd" "diary" entry
               (file+olp+datetree "~/org/files/diary.org")
               "* %^{diary-title} %^G\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pb" "blog idea" entry
               (file+headline "~/org/files/ideas.org" "BLOG")
               "* %^{note-title} :blog:idea:\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pp" "programming idea" entry
               (file+headline "~/org/files/ideas.org" "PROGRAMMING")
               "* %^{note-title} :programming:idea:\n%U\n%?\n"))

(add-to-list 'org-capture-templates
             '("pn" "note" entry
               (file+olp+datetree "~/org/files/notes.org")
               "* %^{note-title} %^G
%U
[[%^C][with more info...]]
%?\n"))

(add-to-list 'org-capture-templates
             '("pt" "todo" entry
               (file+headline "~/org/files/gtd.org" "TODOS")
               "** TODO [/][\%] - %^{task-title}
%U SCHEDULED: %^t DEADLINE: %^t
:PROPERTIES:
:END:
- [ ] %?\n\n"))

(add-to-list 'org-capture-templates
             '("r" "reading templates"))

(add-to-list 'org-capture-templates
             '("rp" "post" entry
               (file+headline "~/org/files/reading.org" "POSTS")
               "* %^{post-title} :post:
- captured on: %U
[[%^C][URL to post...]]
  %?\n"))

(add-to-list 'org-capture-templates
             '("rb" "book" entry
               (file+headline "~/org/files/reading.org" "BOOKS")
               "* %^{book-title} :book:
- captured on: %U
[[%^C][URL to book...]]
%?\n"))


;;; ORG-BULLETS
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(customize-set-variable 'org-bullets-bullet-list
      '("①" "②" "③" "④" "⑤" "⑥" "⑦" "⑧" "⑨"))


;;; ORG-REMARK
(org-remark-global-tracking-mode 1)

(customize-set-variable 'org-remark-notes-file-path "~/org/org-remark/org-remark-notes.org")
(customize-set-variable 'org-remark-tracking-file "~/org/org-remark/.org-remark-tracking")

(org-remark-create "timu-darkcyan"
                   '(:foreground "#2b303b" :background "#5699af" :weight bold))
(org-remark-create "timu-magenta"
                   '(:foreground "#2b303b" :background "#b48ead" :weight bold))
(org-remark-create "timu-orange"
                   '(:foreground "#2b303b" :background "#d08770" :weight bold))
(org-remark-create "timu-yellow"
                   '(:foreground "#2b303b" :background "#ecbe7b" :weight bold))
(set-face-attribute 'org-remark-highlighter nil
                    :foreground "#2b303b" :background "#d08770" :underline nil)


;;; ORG-MODE KEYBINDINGS
(define-key org-mode-map (kbd "M-<left>") nil)
(define-key org-mode-map (kbd "M-<right>") nil)
(define-key org-mode-map (kbd "M-<up>") nil)
(define-key org-mode-map (kbd "M-<down>") nil)

(define-key org-mode-map (kbd "C-<left>") #'org-metaleft)
(define-key org-mode-map (kbd "C-<right>") #'org-metaright)
(define-key org-mode-map (kbd "C-<up>") #'org-metaup)
(define-key org-mode-map (kbd "C-<down>") #'org-metadown)

(define-key evil-normal-state-map (kbd "RET") nil)
(define-key evil-motion-state-map (kbd "RET") nil)
(define-key org-mode-map (kbd "<S-return>") #'timu-func-org-tree-to-right)
(define-key org-mode-map (kbd "M-r") #'timu-func-org-refile-subtree-to-file)

(evil-define-key 'normal org-mode-map (kbd "g r") #'revert-buffer)
(evil-define-key 'normal org-mode-map (kbd "M-l") #'org-insert-link)

(define-key org-capture-mode-map (kbd "M-s") #'org-capture-finalize)
(define-key org-capture-mode-map (kbd "M-w") #'org-capture-kill)

(pcase system-type
  ('darwin
   (define-key org-mode-map (kbd "M-@") #'org-toggle-link-display)
   (define-key org-mode-map (kbd "M-⁄") #'org-toggle-inline-images)
   (define-key org-mode-map (kbd "M-€") #'timu-func-org-toggle-emphasis-markers)
   (define-key org-mode-map (kbd "‘") #'org-edit-special)
   (define-key org-src-mode-map (kbd "‘") #'org-edit-src-exit)
   (define-key org-mode-map (kbd "M-ª") #'timu-func-org-go-to-heading) ; (alt-cmd-h)
   (define-key emacs-lisp-mode-map (kbd "M-ª") #'timu-func-outline-go-to-heading)) ; (alt-cmd-h)
  ('gnu/linux
   (define-key org-mode-map (kbd "C-s-l") #'org-toggle-link-display)
   (define-key org-mode-map (kbd "s-TAB") #'org-toggle-inline-images)
   (define-key org-mode-map (kbd "C-s-e") #'timu-func-org-toggle-emphasis-markers)
   (define-key org-mode-map (kbd "s-#") #'org-edit-special)
   (define-key org-src-mode-map (kbd "s-#") #'org-edit-src-exit)
   (define-key org-mode-map (kbd "C-s-h") #'timu-func-org-go-to-heading) ; (alt-cmd-h)
   (define-key emacs-lisp-mode-map (kbd "C-s-h") #'timu-func-outline-go-to-heading))
  ('windows-nt
   (define-key org-mode-map (kbd "C-s-l") #'org-toggle-link-display)
   (define-key org-mode-map (kbd "C-s-i") #'org-toggle-inline-images)
   (define-key org-mode-map (kbd "C-s-e") #'timu-func-org-toggle-emphasis-markers)
   (define-key org-mode-map (kbd "s-$") #'org-edit-special)
   (define-key org-src-mode-map (kbd "s-$") #'org-edit-src-exit)
   (define-key org-mode-map (kbd "C-s-h") #'timu-func-org-go-to-heading) ; (alt-cmd-h)
   (define-key emacs-lisp-mode-map (kbd "C-s-h") #'timu-func-outline-go-to-heading)))

(pcase system-type
  ('darwin
   (evil-define-key 'normal org-mode-map (kbd "Ø") #'timu-func-insert-line-before)
   (evil-define-key 'normal org-mode-map (kbd "ø") #'timu-func-insert-line-after)
   (evil-define-key 'normal org-mode-map (kbd "ç") #'org-ctrl-c-ctrl-c))
  ((or 'gnu/linux 'windows-nt)
   (evil-define-key 'normal org-mode-map (kbd "s-O") #'timu-func-insert-line-before)
   (evil-define-key 'normal org-mode-map (kbd "s-o") #'timu-func-insert-line-after)
   (evil-define-key 'normal org-mode-map (kbd "s-c") #'org-ctrl-c-ctrl-c)))

(pcase system-type
  ('darwin
   (global-set-key (kbd "M-ç") #'org-capture) ; (alt-cmd-c)
   (global-set-key (kbd "C-M-ç") #'timu-func-make-capture-frame)) ; (ctrl-alt-cmd-c)
  ((or 'gnu/linux 'windows-nt)
   (global-set-key (kbd "C-s-c") #'org-capture) ; (alt-cmd-c)
   (global-set-key (kbd "C-M-s-c") #'timu-func-make-capture-frame)))


(provide 'timu-org)

;;; timu-org.el ends here
