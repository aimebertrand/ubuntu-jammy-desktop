;;; timu-minibuffer.el --- Config for all things minibuffer -*- lexical-binding: t; -*-

;; Author: Aimé Bertrand <aime.bertrand@macowners.club>
;; Version: 3.2
;; Package-Requires: ((emacs "28.1"))
;; Created: 2022-04-06
;; Keywords: tools helper minibuffer
;; Homepage: https://gitlab.com/aimebertrand/dotemacs
;; This file is NOT part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (c) 2022 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; This file provides the config for the minibuffer and any related package.

;;; Code:


(require 'consult)
(require 'embark)
(require 'embark-consult)
(require 'vertico)
(require 'vertico-directory)


;;; DEFAULTS
(defgroup timu-minibuffer ()
  "Customise group for the `timu-minibuffer' Library."
  :group 'timu)

(defcustom timu-minibuffer-path
  (expand-file-name "libraries/timu-minibuffer.el" user-emacs-directory)
  "Variable for the path of the module `timu-minibuffer'."
  :type 'file
  :group 'timu-minibuffer)


;;; WHICH-KEY
(which-key-mode)


;;; VERTICO & CONSULT
;;;; install vertico & co.
(vertico-mode)

(recentf-mode)

;;;; vertico & consul config
(setq completion-category-defaults nil)
(customize-set-variable 'completion-category-overrides '((file (styles partial-completion))))
(customize-set-variable 'consult-async-min-input 1)
(customize-set-variable 'consult-line-start-from-top t)
(customize-set-variable 'completion-styles '(orderless))
(customize-set-variable 'consult-project-root-function #'vc-root-dir)

(pcase system-type
  ('windows-nt
   (customize-set-variable 'consult-find-args
                           "wsl find . -not ( -wholename */.* -prune )")
   (customize-set-variable 'consult-git-grep-args
                           "wsl git --no-pager grep --null --color=never
--ignore-case --extended-regexp --line-number -I")
   (customize-set-variable 'consult-grep-args
                           "wsl grep --null --line-buffered --color=never
--ignore-case --exclude-dir=.git --line-number -I -r .")
   (customize-set-variable 'consult-locate-args
                           "wsl locate --ignore-case --existing")
   (customize-set-variable 'consult-man-args "wsl man -k")
   (customize-set-variable 'consult-ripgrep-args
                           "wsl rg --null --line-buffered --color=never
--max-columns=1000 --path-separator /
--smart-case --no-heading --line-number .")))

;;;; vertico & consult keybindings
(define-key vertico-map (kbd "M-v") #'yank)
(define-key vertico-map (kbd "M-E") #'embark-export)
(define-key vertico-map (kbd "<tab>") #'vertico-directory-enter)
(define-key vertico-map (kbd "<tab>") #'vertico-directory-enter)
(define-key vertico-map (kbd "<escape>") #'abort-recursive-edit)
(define-key minibuffer-local-filename-completion-map (kbd "<backspace>") #'vertico-directory-up)
(define-key minibuffer-local-map (kbd "<backspace>") #'vertico-directory-delete-char)
(define-key minibuffer-local-map (kbd "S-<backspace>") #'vertico-directory-delete-word)
;; Tidy shadowed file names
(add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)

;;;; consult live preview
;; https://github.com/minad/consult#live-previews:
(consult-customize
 consult-ripgrep consult-git-grep consult-grep consult-buffer
 consult-bookmark consult-recent-file consult-xref
 consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
 :preview-key (kbd "M-."))


;;; EMBARK
(customize-set-variable 'embark-prompter 'embark-completing-read-prompter)

(customize-set-variable 'embark-indicators
                        '(embark-minimal-indicator
                          embark-highlight-indicator
                          embark-isearch-highlight-indicator))

(define-key embark-symbol-map (kbd "h") #'helpful-symbol)
(define-key embark-collect-mode-map (kbd "M-<left>") #'evil-window-left)
(define-key embark-collect-mode-map (kbd "M-<right>") #'evil-window-right)
(define-key embark-collect-mode-map (kbd "M-<up>") #'evil-window-up)
(define-key embark-collect-mode-map (kbd "M-<down>") #'evil-window-down)


;;; MARGINALIA
(marginalia-mode)


;;; MINIBUFFER KEYBINDINGS
(define-key minibuffer-local-map (kbd "M-v") #'yank)
(define-key minibuffer-local-map (kbd "<escape>") #'abort-recursive-edit)


(provide 'timu-minibuffer)

;;; timu-minibuffer.el ends here
