(define-package "popper" "20220711.836" "Summon and dismiss buffers as popups"
  '((emacs "26.1"))
  :commit "d7560f18350faaee8362aee16481268de3cc6457" :authors
  '(("Karthik Chikmagalur" . "karthik.chikmagalur@gmail.com"))
  :maintainer
  '("Karthik Chikmagalur" . "karthik.chikmagalur@gmail.com")
  :keywords
  '("convenience")
  :url "https://github.com/karthink/popper")
;; Local Variables:
;; no-byte-compile: t
;; End:
