;;; Generated package description from eldoc-box.el  -*- no-byte-compile: t -*-
(define-package "eldoc-box" "20221008.2315" "Display documentation in childframe" '((emacs "27.1")) :commit "5b893437c8594028653d676015bb580574d9f0e1" :authors '(("Sebastien Chapuis" . "sebastien@chapu.is")) :maintainer '("Yuan Fu" . "casouri@gmail.com") :url "https://github.com/casouri/eldoc-box")
