(define-package "websearch" "20220823.42" "Query search engines"
  '((emacs "24.4"))
  :commit "9336601462ce29822e6aa14db01d923a4bd1c6ef" :authors
  '(("Maciej Barć" . "xgqt@riseup.net"))
  :maintainer
  '("Maciej Barć" . "xgqt@riseup.net")
  :keywords
  '("convenience" "hypermedia")
  :url "https://gitlab.com/xgqt/emacs-websearch/")
;; Local Variables:
;; no-byte-compile: t
;; End:
