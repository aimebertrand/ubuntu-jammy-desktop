(define-package "consult" "20221016.1019" "Consulting completing-read"
  '((emacs "27.1")
    (compat "28.1"))
  :commit "d5b328637255ad6e7da4190ca15f3da040bbb606" :authors
  '(("Daniel Mendler and Consult contributors"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :url "https://github.com/minad/consult")
;; Local Variables:
;; no-byte-compile: t
;; End:
