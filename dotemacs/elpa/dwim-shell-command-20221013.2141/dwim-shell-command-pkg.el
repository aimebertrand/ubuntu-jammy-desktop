(define-package "dwim-shell-command" "20221013.2141" "Shell commands with DWIM behaviour"
  '((emacs "27.1"))
  :commit "45aa7caa40b9326a61c76137da38f4a1dc128d90" :authors
  '(("Alvaro Ramirez"))
  :maintainer
  '("Alvaro Ramirez")
  :url "https://github.com/xenodium/dwim-shell-command")
;; Local Variables:
;; no-byte-compile: t
;; End:
