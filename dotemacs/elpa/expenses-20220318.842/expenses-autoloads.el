;;; expenses-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "expenses" "expenses.el" (0 0 0 0))
;;; Generated autoloads from expenses.el

(register-definition-prefixes "expenses" '("expenses-"))

;;;***

;;;### (autoloads nil "expenses-utils" "expenses-utils.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from expenses-utils.el

(register-definition-prefixes "expenses-utils" '("expenses-utils-"))

;;;***

;;;### (autoloads nil nil ("expenses-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; expenses-autoloads.el ends here
