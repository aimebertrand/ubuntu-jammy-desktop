(define-package "expenses" "20220318.842" "Record and view expenses"
  '((emacs "26.1")
    (dash "2.19.1")
    (ht "2.3"))
  :commit "e668666770858e92de83d8217c7e384de3ba1e34" :authors
  '(("Md Arif Shaikh" . "arifshaikh.astro@gmail.com"))
  :maintainer
  '("Md Arif Shaikh" . "arifshaikh.astro@gmail.com")
  :keywords
  '("expense tracking" "convenience")
  :url "https://github.com/md-arif-shaikh/expenses")
;; Local Variables:
;; no-byte-compile: t
;; End:
