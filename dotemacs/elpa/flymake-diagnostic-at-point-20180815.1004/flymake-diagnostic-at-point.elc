;ELC   
;;; Compiled
;;; in Emacs version 28.2
;;; with all optimizations.



(byte-code "\300\301!\210\300\302!\210\303\304\305\306\307DD\310\311\312\313\314\315\316&	\210\303\317\305\306\320DD\321\311\312\313\322&\210\303\323\305\306\324DD\325\311\312\313\326&\207" [require flymake popup custom-declare-variable flymake-diagnostic-at-point-timer-delay funcall function #[0 "\300\207" [0.5] 1 #1=""] "Delay in seconds before displaying errors at point." :group flymake-diagnostic-at-point :type number :safe numberp flymake-diagnostic-at-point-error-prefix #[0 "\300\207" ["➤ "] 1 #1#] "String to be displayed before every error line." (choice (const :tag "No prefix" nil) string) flymake-diagnostic-at-point-display-diagnostic-function #[0 "\300\207" [flymake-diagnostic-at-point-display-popup] 1 #1#] "The function to be used to display the diagnostic message." (choice (const :tag "Display error messages in a popup" flymake-diagnostic-at-point-display-popup) (const :tag "Display error messages in the minibuffer" flymake-diagnostic-at-point-display-minibuffer) (function :tag "Error display function"))] 10)
#@49 Timer to automatically show the error at point.
(defvar flymake-diagnostic-at-point-timer nil (#$ . 1125))
(make-variable-buffer-local 'flymake-diagnostic-at-point-timer)
#@57 Get the flymake diagnostic text for the thing at point.
(defalias 'flymake-diagnostic-at-point-get-diagnostic-text #[0 "\301`\302\"\303!>\204 \304\305\306D\"\210\211\307H\207" [cl-struct-flymake--diag-tags get-char-property flymake-diagnostic type-of signal wrong-type-argument flymake--diag 5] 5 (#$ . 1302)])
#@64 Display the flymake diagnostic TEXT inside a popup.

(fn TEXT)
(defalias 'flymake-diagnostic-at-point-display-popup #[257 "\301P!\207" [flymake-diagnostic-at-point-error-prefix popup-tip] 4 (#$ . 1623)])
#@67 Display the flymake diagnostic TEXT in the minibuffer.

(fn TEXT)
(defalias 'flymake-diagnostic-at-point-display-minibuffer #[257 "\301P!\207" [flymake-diagnostic-at-point-error-prefix message] 4 (#$ . 1835)])
#@188 Display the flymake diagnostic text for the thing at point.

The diagnostic text will be rendered using the function defined
in `flymake-diagnostic-at-point-display-diagnostic-function.'
(defalias 'flymake-diagnostic-at-point-maybe-display #[0 "\205 \302`\303\"\205 \304 	!\262\207" [flymake-mode flymake-diagnostic-at-point-display-diagnostic-function get-char-property flymake-diagnostic flymake-diagnostic-at-point-get-diagnostic-text] 3 (#$ . 2054)])
#@53 Set the error display timer for the current buffer.
(defalias 'flymake-diagnostic-at-point-set-timer #[0 "\302 \210?\205 \303	\304\305#\211\207" [flymake-diagnostic-at-point-timer flymake-diagnostic-at-point-timer-delay flymake-diagnostic-at-point-cancel-timer run-with-idle-timer nil flymake-diagnostic-at-point-maybe-display] 4 (#$ . 2520) nil])
#@56 Cancel the error display timer for the current buffer.
(defalias 'flymake-diagnostic-at-point-cancel-timer #[0 "\302	\205 \303	!\210\304\211)\207" [inhibit-quit flymake-diagnostic-at-point-timer t cancel-timer nil] 2 (#$ . 2877) nil])
#@76 Set or cancel flymake message display timer after the frame focus changes.
(defalias 'flymake-diagnostic-at-point-handle-focus-change #[0 "\300 \203 \301 \207\302 \207" [frame-focus-state flymake-diagnostic-at-point-set-timer flymake-diagnostic-at-point-cancel-timer] 1 (#$ . 3121)])
#@57 Setup the hooks for `flymake-diagnostic-at-point-mode'.
(defalias 'flymake-diagnostic-at-point-setup #[0 "\301\302\303\304\305$\210\306\307\"\203 \301\310\311\304\305$\210\301\312\303\304\305$\207\313\314\315\316B\317\304$\207" [emacs-version add-hook post-command-hook flymake-diagnostic-at-point-set-timer nil local version< "27.0" focus-out-hook flymake-diagnostic-at-point-cancel-timer focus-in-hook advice--add-function :after #[0 "\300\301!\207" [advice--buffer-local after-focus-change-function] 2] #[257 "\300\301\"\207" [advice--set-buffer-local after-focus-change-function] 4 "\n\n(fn GV--VAL)"] flymake-diagnostic-at-point-handle-focus-change] 5 (#$ . 3412)])
#@58 Remove the hooks for `flymake-diagnostic-at-point-mode'.
(defalias 'flymake-diagnostic-at-point-teardown #[0 "\302\303\304\305#\210\306\307\"\203 \302\310\311\305#\210\302\312\304\305#\207	\313\314\315\"\211\314=?\205) \316\"\207" [emacs-version after-focus-change-function remove-hook post-command-hook flymake-diagnostic-at-point-set-timer local version< "27.0" focus-out-hook flymake-diagnostic-at-point-cancel-timer focus-in-hook advice--remove-function #1=#:v flymake-diagnostic-at-point-handle-focus-change \(setf\ quote\)] 5 (#$ . 4092)])
#@133 Non-nil if Flymake-Diagnostic-At-Point mode is enabled.
Use the command `flymake-diagnostic-at-point-mode' to change this
variable.
(defvar flymake-diagnostic-at-point-mode nil (#$ . 4649))
(make-variable-buffer-local 'flymake-diagnostic-at-point-mode)
#@645 Minor mode for displaying flymake diagnostics at point.

This is a minor mode.  If called interactively, toggle the
`Flymake-Diagnostic-At-Point mode' mode.  If the prefix argument
is positive, enable the mode, and if it is zero or negative,
disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `flymake-diagnostic-at-point-mode'.

The mode's hook is called both when the mode is enabled and when
it is disabled.

(fn &optional ARG)
(defalias 'flymake-diagnostic-at-point-mode #[256 "\302 \303=\203 ?\202 \247\203 \304W\203 \305\202 \306\307\301!\2031 \310\300	\"\2031 \300	B\203; \311 \210\202> \312 \210\313\314\203H \315\202I \316\"\210\317\320!\203k \302 \203] \211\302 \232\203k \321\322\203g \323\202h \324\325#\210\210\326 \210\207" [flymake-diagnostic-at-point-mode local-minor-modes current-message toggle 1 nil t boundp delq flymake-diagnostic-at-point-setup flymake-diagnostic-at-point-teardown run-hooks flymake-diagnostic-at-point-mode-hook flymake-diagnostic-at-point-mode-on-hook flymake-diagnostic-at-point-mode-off-hook called-interactively-p any message "Flymake-Diagnostic-At-Point mode %sabled%s" "en" "dis" " in current buffer" force-mode-line-update] 6 (#$ . 4909) (byte-code "\203\n \301!\202 \302C\207" [current-prefix-arg prefix-numeric-value toggle] 2)])
(defvar flymake-diagnostic-at-point-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\303\301\305\306#\210\303\301\307\310C#\210\311\312\310\313\300!\205# \310\211%\210\314\315!\207" [flymake-diagnostic-at-point-mode-map flymake-diagnostic-at-point-mode-hook variable-documentation put "Hook run after entering or leaving `flymake-diagnostic-at-point-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" custom-type hook standard-value nil add-minor-mode flymake-diagnostic-at-point-mode boundp provide flymake-diagnostic-at-point] 6)
