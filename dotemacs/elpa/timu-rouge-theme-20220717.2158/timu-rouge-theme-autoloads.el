;;; timu-rouge-theme-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "timu-rouge-theme" "timu-rouge-theme.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from timu-rouge-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(register-definition-prefixes "timu-rouge-theme" '("timu-rouge"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; timu-rouge-theme-autoloads.el ends here
