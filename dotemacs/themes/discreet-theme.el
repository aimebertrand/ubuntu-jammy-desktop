;;; discreet-theme.el --- A discreet dark theme for Emacs -*- lexical-binding:t -*-

;; Maintainer: Aimé Bertrand <aime.bertrand@macowners.club>
;; Keywords: faces themes
;; Package-Requires: ((emacs "27.1"))

;; This file is not part of GNU Emacs.

;; The MIT License (MIT)
;;
;; Copyright (C) 2021 Aimé Bertrand
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:
;; Original Theme by tonyaldon - https://github.com/tonyaldon/emacs.d
;; Copied it from his Emacs Config. Adjusted some colors for myself.
;; Attributed the MIT License like the original:
;; https://github.com/tonyaldon/emacs.d/blob/master/LICENSE

;;; Code:

(deftheme discreet  "A discreet dark theme for Emacs")

(custom-theme-set-variables 'discreet)

(let ((d-black-1       "#151515")
      (d-black-2       "#161a1f")
      (d-black-3       "#222222")
      (d-black-4       "#333333")
      (d-black-5       "#394851")
      (d-gray-1        "#555555")
      (d-gray-2        "#5e5e5e")
      (d-gray-3        "#8c8c8c")
      (d-gray-4        "#b3b3b3")
      (d-white-0       "#ffffff")
      (d-white-1       "#dedede")
      (d-red           "#ff6c60")
      (d-orange-1      "#fd721f")
      (d-orange-2      "#fd971f")
      (d-orange-3      "#feb259")
      (d-yellow-1      "#ffd500")       ; not used here (for cursor)
      (d-yellow-2      "#eedc82")
      (d-yellow-3      "#f5ebb6")
      (d-green-1       "#60ff6c")
      (d-green-2       "#26f9ad")
      (d-green-3       "#59feb2")
      (d-green-4       "#afad00")
      (d-aquamarine-1  "#7fffd4")
      (d-aquamarine-2  "#15bb84")
      (d-aquamarine-3  "#359b79")
      (d-aquamarine-4  "#458b74")
      (d-cyan-1        "#457f8b")
      (d-cyan-2        "#5297a5")
      (d-cyan-3        "#02eaf3")
      (d-cyan-4        "#017a7e")
      (d-blue-1        "#87cefa")
      (d-blue-2        "#8795fa")       ; not used here (for cursor)
      (d-blue-3        "#59a5fe")       ; not used here (for cursor)
      (d-pink-1        "#fa87ce")       ; not used here (for cursor)
      (d-pink-2        "#f92672"))


  (custom-theme-set-faces
   'discreet

;;;; defaults
   `(bold ((t (:bold t))))
   `(default ((t (:background ,d-black-1 :foreground ,d-white-1))))
   `(hl-line ((t (:background ,d-black-3 ))))
   `(cursor ((t (:background ,d-green-2))))

   `(highlight ((t (:foreground ,d-white-0 :bold t))))
   `(lazy-highlight ((t (:underline ,d-orange-2 :bold t))))
   `(region ((t (:background ,d-black-5))))

   `(error ((t (:foreground ,d-red))))
   `(warning ((t (:foreground ,d-orange-1))))
   `(success ((t (:foreground ,d-green-1))))
   `(match ((t (:foreground ,d-orange-2 :weight bold))))


;;;; iedit
   `(isearch ((t (:inherit highlight :underline t))))
   `(isearch-fail ((t (:background ,d-red))))

;;;; iedit
   `(iedit-occurrence ((t (:underline ,d-orange-2 :bold t))))

;;;; whitespace
   `(whitespace-tab ((t (:background ,d-green-4 :foreground ,d-black-1))))
   `(whitespace-line ((t (:background ,d-black-1 :foreground ,d-pink-1))))

;;;; avy
   `(avy-lead-face ((t (:foreground ,d-red :weight bold))))
   `(avy-lead-face-0 ((t (:inherit avy-lead-face))))
   `(avy-lead-face-1 ((t (:inherit avy-lead-face))))

;;;; minibuffer
   `(minibuffer-prompt ((t (:foreground ,d-pink-2 :bold t))))

;;;; dired
   `(dired-directory ((t (:foreground ,d-aquamarine-2 :weight bold :underline t))))
   `(dired-flagged ((t (:inherit error))))
   `(dired-header ((t (:inherit ta-dired-header-face))))
   `(dired-ignored ((t (:inherit shadow))))
   `(dired-mark ((t (:inherit font-lock-variable-name-face))))
   `(dired-marked ((t (:inherit font-lock-variable-name-face))))
   `(dired-perm-write ((t (:inherit font-lock-comment-delimiter-face))))
   `(dired-symlink ((t (:foreground ,d-yellow-2))))
   `(dired-warning ((t (:inherit font-lock-warning-face))))

;;;; embark
   `(embark-collect-candidate ((t (:inherit highlight :underline ,d-pink-1 :extend t))))
   `(embark-keybinding ((t (:inherit success))))

;;;; vertico
   `(vertico-current ((t (:inherit highlight :underline ,d-pink-1 :extend t))))
   `(vertico-group-title ((t (:inherit shadow :slant italic))))
   `(vertico-group-separator ((t (:inherit shadow :strike-through t))))
   `(vertico-multiline ((t (:inherit shadow))))

;;;; mode-line
   `(mode-line ((t (:background ,d-black-3 :foreground ,d-aquamarine-1))))
   `(mode-line-inactive ((t (:background ,d-gray-1 :foreground ,d-gray-3))))
   `(mode-line-buffer-id ((t (:foreground ,d-gray-4 :weight bold))))

   `(info-xref ((t (:foreground ,d-aquamarine-3 :underline t))))
   `(info-xref-visited ((t (:foreground ,d-yellow-3 :underline t))))
   `(info-header-xref ((t (:foreground ,d-white-1 :underline t))))
   `(info-menu-star ((t (:foreground ,d-white-1))))
   `(link ((t (:foreground ,d-aquamarine-3 :underline t))))

   `(wgrep-done-face ((t (:foreground ,d-blue-1 :weight bold))))
   `(wgrep-face ((t (:underline (:color ,d-gray-4 :style wave)))))
   `(wgrep-file-face ((t (:background ,d-gray-2 :foreground ,d-white-1))))
   `(wgrep-reject-face ((t (:foreground ,d-pink-2 :weight bold))))

   `(compilation-error ((t (:foreground ,d-red))))
   `(compilation-info ((t (:foreground ,d-cyan-2 :underline t))))
   `(compilation-line-number ((t (:foreground ,d-yellow-2 :underline t))))
   `(compilation-warning ((t (:foreground ,d-orange-2))))
   `(compilation-mode-line-exit ((t (:foreground ,d-green-1))))
   `(compilation-mode-line-fail ((t (:foreground ,d-red))))
   `(compilation-mode-line-run ((t (:foreground ,d-orange-2))))

   `(company-preview ((t (:foreground ,d-white-1 :inherit hl-line :bold t))))
   `(company-preview-common ((t (:foreground ,d-white-1 :inherit hl-line :bold t))))
   `(company-preview-search ((t (:foreground ,d-orange-2 :inherit hl-line :bold t))))
   `(company-scrollbar-bg ((t (:background ,d-black-4))))
   `(company-scrollbar-fg ((t (:inherit highlight))))
   `(company-tooltip ((t (:foreground ,d-gray-3 :background ,d-black-1))))
   `(company-tooltip-common ((t (:foreground ,d-cyan-4 :weight bold))))
   `(company-tooltip-common-selection ((t (:foreground ,d-cyan-3 :weight bold))))
   `(company-tooltip-selection ((t (:foreground ,d-white-1 :bold t))))
   `(company-tooltip-annotation ((t (:foreground ,d-orange-2))))
   `(company-tooltip-annotation-selection ((t (:foreground ,d-orange-2))))
   `(company-tooltip-search ((t (:foreground ,d-orange-2 :bold t))))
   `(company-tooltip-search-selection ((t (:foreground ,d-orange-2 :bold t))))

   `(ivy-current-match ((t (:underline ,d-white-1 :bold t))))
   `(ivy-cursor ((t (:background ,d-white-1))))
   `(ivy-minibuffer-match-face-1 ((t (:underline ,d-orange-2 :bold t))))
   `(ivy-minibuffer-match-face-2 ((t (:underline ,d-orange-3 :foreground ,d-white-0 :bold t))))
   `(ivy-minibuffer-match-face-3 ((t (:underline ,d-green-3 :foreground ,d-white-0 :bold t))))
   `(ivy-minibuffer-match-face-4 ((t (:underline ,d-blue-3 :foreground ,d-white-0 :bold t))))
   `(ivy-prompt-match ((t (:inherit ivy-current-match))))

   `(swiper-background-match-face-1 ((t (:underline ,d-orange-2 :bold t))))
   `(swiper-background-match-face-2 ((t (:underline ,d-orange-3 :foreground ,d-white-0 :bold t))))
   `(swiper-background-match-face-3 ((t (:underline ,d-green-3 :foreground ,d-white-0 :bold t))))
   `(swiper-background-match-face-4 ((t (:underline ,d-blue-3 :foreground ,d-white-0 :bold t))))
   `(swiper-isearch-current-match ((t (:background "black" :foreground "white"))))
   `(swiper-line-face ((t (:underline ,d-white-1 :bold t))))
   `(swiper-match-face-1 ((nil)))
   `(swiper-match-face-2 ((nil)))
   `(swiper-match-face-3 ((nil)))
   `(swiper-match-face-4 ((nil)))

   `(counsel--mark-ring-highlight ((t (:inherit highlight))))
   `(counsel-application-name ((t (:inherit font-lock-builtin-face))))
   `(counsel-key-binding ((t (:inherit font-lock-keyword-face))))

;;;; font-lock
   `(font-lock-negation-char-face ((t (:foreground ,d-red))))
   `(font-lock-warning-face ((t (:foreground ,d-orange-2))))
   `(font-lock-variable-name-face ((t (:foreground ,d-orange-2))))
   `(font-lock-doc-face ((t (:foreground ,d-yellow-2))))
   `(font-lock-doc-string-face ((t (:foreground ,d-yellow-2))))
   `(font-lock-string-face ((t (:foreground ,d-yellow-2))))
   `(font-lock-function-name-face ((t (:foreground ,d-aquamarine-4))))
   `(font-lock-builtin-face ((t (:foreground ,d-cyan-1))))
   `(font-lock-type-face ((t (:foreground ,d-cyan-2))))
   `(font-lock-keyword-face ((t (:foreground ,d-pink-2))))
   `(font-lock-preprocessor-face ((t (:foreground ,d-pink-2))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,d-gray-3))))
   `(font-lock-comment-face ((t (:foreground ,d-gray-3))))
   `(font-lock-constant-face ((t (:foreground ,d-blue-1))))
   `(font-lock-reference-face ((t (:foreground ,d-yellow-3))))
   `(font-lock-regexp-grouping-backslash ((t (:foreground ,d-blue-1))))
   `(font-lock-regexp-grouping-construct ((t (:foreground ,d-blue-1))))
   `(font-lock-number-face ((t (:foreground ,d-yellow-2))))

;;;; mu4e - dark
   `(mu4e-header-key-face ((t (:foreground ,d-aquamarine-1))))
   `(mu4e-highlight-face ((t (:foreground ,d-black-1 :background ,d-pink-1))))
   `(mu4e-title-face ((t (:foreground ,d-blue-1))))
   `(mu4e-unread-face ((t (:foreground ,d-pink-1))))
   `(mu4e-header-title-face ((t (:foreground ,d-blue-1))))
   `(mu4e-replied-face ((t (:foreground ,d-aquamarine-1))))

;;;; org-mode
   `(org-mode-line-clock ((t (:inherit mode-line))))
   `(org-mode-line-clock-overrun ((t (:inherit mode-line :background ,d-red))))
   `(org-block ((t (:foreground ,d-gray-4 :background ,d-black-2 :extend t))))
   `(org-block-background ((t (:foreground ,d-gray-3 :background ,d-black-2 :extend t))))
   `(org-block-begin-line ((t (:foreground ,d-gray-3 :slant italic :background ,d-black-2 :extend t))))
   `(org-block-end-line ((t (:inherit org-block-begin-line))))
   `(org-checkbox ((t (:foreground ,d-aquamarine-1))))
   `(org-ellipsis ((t (:underline nil :background nil :foreground ,d-gray-3))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'discreet)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; discreet-theme.el ends here
