FROM kasmweb/core-ubuntu-focal:1.11.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

RUN add-apt-repository ppa:kelleyk/emacs

RUN apt update && \
    apt install -y \
    apt-utils \
    bat \
    curl \
    emacs28 \
    firefox \
    fzf \
    git \
    gnupg \
    htop \
    neovim \
    ripgrep \
    silversearcher-ag \
    sqlite \
    sqlite3 \
    wget \
    zsh \
    zsh-autosuggestions \
    zsh-syntax-highlighting

RUN curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | bash -s install deb-get

RUN deb-get install fd git-delta bat

ADD dotfiles /root/.dotfiles

ADD dotemacs /root/.emacs.d

RUN mkdir -p /root/.config && \
    ln -s /root/.dotfiles/bat /root/.config/ && \
    ln -s /root/.dotfiles/git/config /root/.gitconfig && \
    ln -s /root/.dotfiles/nvim /root/.config/ && \
    ln -s /root/.dotfiles/zsh/zshrc /root/.zshrc

RUN vim -E +PlugUpgrade +PlugInstall +qall || true

ADD dotfiles /home/kasm-user/.dotfiles

ADD dotemacs /home/kasm-user/.emacs.d

ADD local /home/kasm-user/.local

RUN mkdir -p /home/kasm-user/.config && \
    ln -s /home/kasm-user/.dotfiles/bat /home/kasm-user/.config/ && \
    ln -s /home/kasm-user/.dotfiles/git/config /home/kasm-user/.gitconfig && \
    ln -s /home/kasm-user/.dotfiles/nvim /home/kasm-user/.config/ && \
    ln -s /home/kasm-user/.dotfiles/zsh/zshrc /home/kasm-user/.zshrc

RUN echo "kasm-user ALL=(ALL:ALL) ALL" > /etc/sudoers.d/kasm-user && \
    echo "kasm-user:abc" | chpasswd && \
    chmod 0440 /etc/sudoers.d/kasm-user

######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
